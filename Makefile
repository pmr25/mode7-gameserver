PROTO_SRC_DIR=./assets/protobuf
GO_DST_DIR=.
CPP_DST_DIR=./lib/protocol
VERSION=1.0
ARCH=`uname -m`

all: server bridge

release: all
	mkdir ./rel
	mkdir ./rel/include
	mkdir ./rel/lib
	mkdir ./rel/bin
	cp mode7-gameserver rel/bin
	cp lib/protocol/*.h rel/include
	cp lib/protocol/*.so rel/lib
	cd rel; zip -b ../ -r mode7-gameserver-$(ARCH)-$(VERSION).zip ./*; mv *.zip ../
	rm -rf rel

protocol_go:
	protoc -I=$(PROTO_SRC_DIR) --go_out=$(GO_DST_DIR) $(PROTO_SRC_DIR)/*.proto

protocol_cpp:
	protoc -I=$(PROTO_SRC_DIR) --cpp_out=$(CPP_DST_DIR) $(PROTO_SRC_DIR)/*.proto
	VERSION=$(VERSION) make -C $(CPP_DST_DIR)

GameNetworkingSockets: protocol_go
	mkdir -p 3rdparty/GameNetworkingSockets/build
	cmake -S 3rdparty/GameNetworkingSockets -B 3rdparty/GameNetworkingSockets/build  -G Ninja
	ninja -C 3rdparty/GameNetworkingSockets/build

server: protocol_go
	go build -o server -tags server

bridge: protocol_go
	go build -o bridge -tags bridge

.PHONY: test
test:
	go test -v ./... -tags server bridge

.PHONY: mocks
mocks:
	cd internal && mockery --all

.PHONY: clean
clean:
	rm -f server
	rm -f bridge
	rm -f ./pkg/protocol/*.pb.go
	VERSION=$(VERSION) make -C $(CPP_DST_DIR) clean

.PHONY: netcat-udp
netcat-udp:
	nc -u localhost 10262

.PHONY: netcat-tcp
netcat-tcp:
	nc -t localhost 26290

.PHONY: format
format:
	gofmt -l -w .
