module gitlab.com/pmr25/mode7-gameserver

go 1.19

require (
	github.com/google/uuid v1.3.0
	github.com/mattn/go-sqlite3 v1.14.17
	github.com/stretchr/testify v1.8.4
	github.com/zenazn/pkcs7pad v0.0.0-20170308005700-253a5b1f0e03
	go.uber.org/fx v1.19.2
	go.uber.org/zap v1.23.0
	google.golang.org/protobuf v1.30.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/objx v0.5.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/dig v1.16.1 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/sys v0.5.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
