//go:build server

package state

import (
	"errors"

	"gitlab.com/pmr25/mode7-gameserver/pkg/protocol"
	"go.uber.org/zap"
)

type GameState struct {
	log       *zap.Logger
	clientMap map[string]int
	clients   []*Client
}

func NewGameState(log *zap.Logger) *GameState {
	return &GameState{
		log:       log,
		clientMap: make(map[string]int),
		clients:   make([]*Client, 0),
	}
}

func (s *GameState) Write(packet *protocol.GamePacket) error {
	if len(packet.VehicleStates) != 1 {
		return errors.New("no vehicle states")
	}

	key := packet.Packet.Token.Id
	idx, ok := s.clientMap[key]
	if !ok {
		return errors.New("client does not exist")
	}

	if key != packet.VehicleStates[0].Id {
		return errors.New("wrong id")
	}

	client := s.clients[idx]
	client.LastState = packet.VehicleStates[0]
	return nil
}

func (s *GameState) Read() *protocol.GamePacket {
	output := protocol.NewGamePacket(protocol.PACKET_TYPE_SERVER_STATE)
	for _, client := range s.clients {
		output.VehicleStates = append(output.VehicleStates, client.LastState)
	}
	return output
}

func (s *GameState) GetClients() []*Client {
	return s.clients
}

func (s *GameState) GetClientById(id string) *Client {
	idx, ok := s.clientMap[id]
	if !ok {
		return nil
	}

	return s.clients[idx]
}

func (s *GameState) RegisterClient(client *Client) error {
	key := client.Id.String()

	_, ok := s.clientMap[key]
	if ok {
		return errors.New("client already registered")
	}

	s.clientMap[key] = len(s.clients)
	s.clients = append(s.clients, client)

	return nil
}

func (s *GameState) RemoveClient(id string) error {
	index, ok := s.clientMap[id]
	if !ok {
		return errors.New("client does not exist")
	}

	delete(s.clientMap, id)
	s.clients[index] = nil
	s.ReorderClients()

	return nil
}

func (s *GameState) ReorderClients() {
	newClientList := make([]*Client, 0, len(s.clients)-1)
	newClientMap := make(map[string]int)
	for k, v := range s.clientMap {
		client := s.clients[v]
		if client == nil {
			continue
		}
		newClientMap[k] = len(newClientList)
		newClientList = append(newClientList, client)
	}
	s.clientMap = newClientMap
	s.clients = newClientList
}
