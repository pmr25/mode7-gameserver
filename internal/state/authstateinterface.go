package state

//go:generate mockery --name AuthStateInterface
type AuthStateInterface interface {
	GetUserKey(string) (string, error)
}
