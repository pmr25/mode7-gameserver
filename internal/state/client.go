package state

import (
	"net"

	"github.com/google/uuid"
	"gitlab.com/pmr25/mode7-gameserver/pkg/protocol"
)

type Client struct {
	Id        uuid.UUID
	Addr      *net.UDPAddr
	LastState *protocol.VehicleState
}

func NewClient(addr *net.UDPAddr, id string) (*Client, error) {
	parsedId, err := uuid.Parse(id)
	if err != nil {
		return nil, err
	}

	return &Client{
		Addr: addr,
		Id:   parsedId,
	}, nil
}
