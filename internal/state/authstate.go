//go:build server

package state

import (
	"database/sql"
	"embed"
	"os"
	"errors"

	_ "github.com/mattn/go-sqlite3"

	"go.uber.org/zap"
)

//go:embed auth/migrations/*
var migrations embed.FS


type AuthState struct {
	log *zap.Logger
}

func bootstrapDatabase(log *zap.Logger) {
	path := "./authserver.db"
	var _, err = os.Stat(path)
	if os.IsNotExist(err) {
		log.Sugar().Warn("creating new database")
		fp, err := os.Create(path)
		if err != nil {
			panic(err)
		}
		defer fp.Close()
	}
}

func runMigration(db *sql.DB, queries string) {
	query, err := db.Prepare(queries)
	if err != nil {
		panic(err)
	}
	query.Exec()
}

func runMigrations(log *zap.Logger) {
	db, err := sql.Open("sqlite3", "./authserver.db")
	if err != nil {
		panic(err)
	}

	workdir := "auth/migrations"
	entires, err := migrations.ReadDir(workdir)
	if err != nil {
		panic(err)
	}
	for _, entry := range entires {
		if entry.IsDir() {
			continue
		}

		queries, err := migrations.ReadFile(workdir + "/" + entry.Name())
		if err != nil {
			panic(err)
		}

		log.Sugar().Info("running ", entry.Name())
		runMigration(db, string(queries))
	}
}

func NewAuthState(log *zap.Logger) *AuthState {
	bootstrapDatabase(log)
	runMigrations(log)

	return &AuthState{
		log: log,
	}
}

func (s *AuthState) GetUserKey(username string) (string, error) {
	db, err := sql.Open("sqlite3", "./authserver.db")
	if err != nil {
		return "", errors.New("cannot open db")
	}
	query := `SELECT key FROM passwords JOIN users ON passwords.user_id = users.id WHERE users.username = ?`
	statement, err := db.Prepare(query)
	if err != nil {
		return "", errors.New("cannot get user key")
	}

	row, err := statement.Query(username)
	if err != nil {
		return "", errors.New("cannot run query")
	}

	defer row.Close()
	for row.Next() {
		var key string
		row.Scan(&key)
		return key, nil
	}

	return "", errors.New("could not find key")
}
