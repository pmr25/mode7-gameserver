package service

//go:generate mockery --name cryptointerface
type CryptoInterface interface {
	GetRandomBytes(n int) ([]byte, error)
	MakeChallenge(solution, key []byte) ([]byte, []byte, error)
	CheckChallengeSoln(proposal, solution []byte) (bool, error)
}