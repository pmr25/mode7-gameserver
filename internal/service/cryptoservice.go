package service

import (
	"bytes"
	"crypto/aes"
	"crypto/rand"
	"crypto/sha256"

	"github.com/zenazn/pkcs7pad"
	"go.uber.org/zap"
)

type CryptoService struct {
	log *zap.Logger
}

func NewCryptoService(log *zap.Logger) *CryptoService {
	return &CryptoService{
		log: log,
	}
}

func (c *CryptoService) GetRandomBytes(n int) ([]byte, error) {
	material := make([]byte, n)
	_, err := rand.Read(material)
	if err != nil {
		return nil, err
	}
	return material, nil
}
	
func (c *CryptoService) MakeChallenge(answer, key []byte) ([]byte, []byte, error) {
	cipher, err := aes.NewCipher(key)
	if err != nil {
		return nil, nil, err
	}

	paddedAnswer := pkcs7pad.Pad(answer, cipher.BlockSize())

	question := make([]byte, cipher.BlockSize())
	cipher.Encrypt(question, paddedAnswer)

	answerHash := sha256.New()
	answerHash.Write(answer)

	return question, answerHash.Sum(nil), nil
}

func (c *CryptoService) CheckChallengeSoln(proposal, answerHash []byte) (bool, error) {
	proposalHash := sha256.New()
	proposalHash.Write(proposal)

	return bytes.Equal(proposalHash.Sum(nil), answerHash), nil
}
