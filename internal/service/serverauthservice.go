//go:build server

package service

import (
	"bytes"
	"crypto/aes"
	"crypto/ecdh"
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"fmt"

	"gitlab.com/pmr25/mode7-gameserver/internal/state"
	"gitlab.com/pmr25/mode7-gameserver/pkg/protocol"
	"go.uber.org/zap"
)

type ServerAuthService struct {
	log  *zap.Logger
	crypto CryptoInterface
	repo state.AuthStateInterface
}

func NewServerAuthService(log *zap.Logger, crypto CryptoInterface, repo state.AuthStateInterface) *ServerAuthService {
	return &ServerAuthService{
		log:  log,
		crypto: crypto,
		repo: repo,
	}
}

func (s *ServerAuthService) TokenRequest(request *protocol.AuthPacket) (*protocol.AuthPacket, error) {
	return protocol.CreateAuthPacket(
		protocol.PACKET_TYPE_TOKEN_ISSUE,
		"secret",
		request.Packet.Sequence+1,
	), nil
}

func (s *ServerAuthService) DHKeyExchange(request *protocol.AuthPacket) (*protocol.AuthPacket, error) {

	serverKey, err := ecdh.P256().GenerateKey(rand.Reader)
	if err != nil {
		return nil, err
	}
	serverPublicKey, err := ecdh.P256().NewPublicKey(serverKey.PublicKey().Bytes())
	if err != nil {
		return nil, err
	}

	clientMaterial, err := base64.StdEncoding.DecodeString(request.DhKeyExch.GetClientMaterial())
	if err != nil {
		return nil, err
	}
	clientPublicKey, err := x509.ParsePKIXPublicKey(clientMaterial)
	if err != nil {
		return nil, err
	}
	clientECDH, err := (clientPublicKey.(*ecdsa.PublicKey)).ECDH()
	if err != nil {
		return nil, err
	}

	secret, err := serverKey.ECDH(clientECDH)
	if err != nil {
		return nil, err
	}
	fmt.Println("secret", secret)

	response := protocol.CreateAuthPacket(protocol.PACKET_TYPE_DH_KEY_EXCH, "", request.GetPacket().GetSequence()+1)
	response.DhKeyExch = &protocol.DHKeyExch{}

	marshalledPublicKey, err := x509.MarshalPKIXPublicKey(serverPublicKey)
	if err != nil {
		return nil, err
	}
	response.DhKeyExch.ServerMaterial = base64.StdEncoding.EncodeToString(marshalledPublicKey)

	return response, nil
}

func (s *ServerAuthService) ChallengeRequest(userName string) (string, string, error) {
	encodedKey, err := s.repo.GetUserKey(userName)
	if err != nil {
		return "", "", err
	}
	key, err := base64.StdEncoding.DecodeString(encodedKey)
	if err != nil {
		return "", "", err
	}

	cipher, err := aes.NewCipher(key)
	if err != nil {
		return "", "", err
	}
	
	solution := make([]byte, cipher.BlockSize())
	_, err = rand.Read(solution)
	if err != nil {
		return "", "", err
	}
	hash := sha256.New()
	hash.Write(solution)

	challenge := make([]byte, cipher.BlockSize())
	cipher.Encrypt(challenge, solution)

	return base64.StdEncoding.EncodeToString(challenge), base64.RawStdEncoding.EncodeToString(hash.Sum(nil)), nil
}

func (s *ServerAuthService) ChallengeSolution(encodedSoln, encodedHash string) (bool, error) {
	decodedSoln, err := base64.StdEncoding.DecodeString(encodedSoln)
	if err != nil {
		return false, err
	}
	decodedHash, err := base64.StdEncoding.DecodeString(encodedHash)
	if err != nil {
		return false, err
	}
	hash := sha256.New()
	hash.Write(decodedSoln)

	isSolved := bytes.Compare(hash.Sum(nil), decodedHash) == 0
		
	return isSolved, nil
}

func (s *ServerAuthService) Health(request *protocol.AuthPacket) (*protocol.AuthPacket, error) {
	return protocol.CreateAuthPacket(protocol.PACKET_TYPE_NOTICE, "ok", request.GetPacket().GetSequence()+1), nil
}
