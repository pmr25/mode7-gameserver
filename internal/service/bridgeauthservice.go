//go:build bridge

package service

import "go.uber.org/zap"

type BridgeAuthService struct {
	log *zap.Logger
}

func NewBridgeAuthService(log *zap.Logger) *BridgeAuthService {
	return &BridgeAuthService{
		log: log,
	}
}
