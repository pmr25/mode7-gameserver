//go:build server

package service

import (
	"errors"
	"net"
	"time"

	"gitlab.com/pmr25/mode7-gameserver/internal/state"
	"gitlab.com/pmr25/mode7-gameserver/pkg/protocol"
	"go.uber.org/zap"
)

type ServerGameService struct {
	log   *zap.Logger
	state *state.GameState
}

func NewServerGameService(log *zap.Logger, gstate *state.GameState) *ServerGameService {
	return &ServerGameService{
		log:   log,
		state: gstate,
	}
}

func (s *ServerGameService) GetClients() []*net.UDPAddr {
	clientList := s.state.GetClients()
	numClients := len(clientList)
	clientAddrs := make([]*net.UDPAddr, 0, numClients)

	for i := 0; i < numClients; i++ {
		if clientList[i] == nil {
			panic("nil client")
		}
		clientAddrs = append(clientAddrs, clientList[i].Addr)
	}

	return clientAddrs
}

func (s *ServerGameService) ValidateToken(packet *protocol.GamePacket) error {
	if packet == nil {
		return errors.New("nil packet")
	}
	if packet.Packet == nil {
		return errors.New("nil packet packet")
	}
	if packet.Packet.Token == nil {
		return errors.New("nil token")
	}

	token := packet.Packet.Token
	if token.GrantedAt.AsTime().After(time.Now()) {
		return errors.New("bad token")
	}
	if token.ExpiresAt.AsTime().Before(time.Now()) {
		return errors.New("token expired")
	}
	if token.Material != "secret" {
		return errors.New("bad token secret")
	}

	return nil
}

func (s *ServerGameService) ValidateClient(id string, addr *net.UDPAddr) bool {
	if addr == nil {
		return false
	}

	found := s.state.GetClientById(id)
	if found == nil {
		s.log.Sugar().Warnf("client with id %v not found", id)
		return false
	}

	return s.state.GetClientById(id).Addr.IP.Equal(addr.IP)
}

func (s *ServerGameService) ProcessPacket(packet *protocol.GamePacket, addr *net.UDPAddr) error {
	if err := s.ValidateToken(packet); err != nil {
		return err
	}
	if !s.ValidateClient(packet.Packet.Token.Id, addr) {
		return errors.New("unknown client")
	}

	s.WriteVehicleState(packet)

	return nil
}

func (s *ServerGameService) GetState() *protocol.GamePacket {
	return s.state.Read()
}

func (s *ServerGameService) RegisterClient(packet *protocol.GamePacket, addr *net.UDPAddr) (*protocol.GamePacket, error) {
	id := packet.Packet.Token.Id
	s.log.Sugar().Infof("register new client %v", id)

	client, err := state.NewClient(addr, id)
	if err != nil {
		return nil, err
	}
	err = s.state.RegisterClient(client)
	if err != nil {
		return nil, err
	}

	s.log.Sugar().Infof("%d clients connected", len(s.GetClients()))

	return protocol.NewGamePacket(protocol.PACKET_TYPE_JOIN_GAME), nil
}

func (s *ServerGameService) RemoveClient(packet *protocol.GamePacket) error {
	key := packet.Packet.Token.Id
	s.log.Sugar().Infof("remove client %s", key)
	return s.state.RemoveClient(key)
}

func (s *ServerGameService) WriteVehicleState(packet *protocol.GamePacket) error {
	return s.state.Write(packet)
}
