package test

import (
	"bytes"
	"crypto/aes"
	"crypto/sha256"
	"errors"

	"github.com/zenazn/pkcs7pad"
	"gitlab.com/pmr25/mode7-gameserver/internal/service"
	"go.uber.org/zap"

	"testing"
)

func Test_GetRandomBytes(t *testing.T) {
	// arrange
	crypto := service.NewCryptoService(zap.NewExample())

	// act
	result, err := crypto.GetRandomBytes(10)

	// assert
	if err != nil {
		t.Error(err)
	}
	if len(result) != 10 {
		t.Error(errors.New("wrong length"))
	}
}

func Test_MakeChallenge(t *testing.T) {
	// arrange
	crypto := service.NewCryptoService(zap.NewExample())

	answer := []byte("testing")
	key := []byte("abcdefghijklmnop")

	// act
	question, answerHash, err := crypto.MakeChallenge(answer, key)

	// assert
	if err != nil {
		t.Error(err)
	}

	testHash := sha256.New()
	testHash.Write(answer)
	if !bytes.Equal(testHash.Sum(nil), answerHash){
		t.Error(errors.New("hash does not match"))
	}

	if question == nil {
		t.Error(errors.New("question is nil"))
	}
}

func Test_CheckChallengeSoln(t *testing.T) {
	// arrange
	crypto := service.NewCryptoService(zap.NewExample())

	answer := []byte("testing")
	key := []byte("abcdefghijklmnop")

	// act
	question, answerHash, err := crypto.MakeChallenge(answer, key)
	
	cipher, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	paddedSolution := make([]byte, cipher.BlockSize())
	cipher.Decrypt(paddedSolution, question)
	solution, err := pkcs7pad.Unpad(paddedSolution)
	if err != nil {
		t.Error("could not unpad")
	}

	isSolved, err := crypto.CheckChallengeSoln(solution, answerHash)
	if err != nil {
		t.Error(err)
	}
	if !isSolved {
		t.Error("should have solved")
	}

	wrongAnswer, err := crypto.CheckChallengeSoln([]byte("wrong"), answerHash)
	if err != nil {
		t.Error(err)
	}
	if wrongAnswer {
		t.Error("should have not solved")
	}

	// assert
}