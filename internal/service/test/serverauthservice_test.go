package test

import (
	"crypto/ecdh"
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/x509"
	"encoding/base64"
	"fmt"
	"testing"

	"gitlab.com/pmr25/mode7-gameserver/internal/service"
	"gitlab.com/pmr25/mode7-gameserver/internal/mocks"
	"gitlab.com/pmr25/mode7-gameserver/pkg/protocol"
	"go.uber.org/zap"
)

func TestAuthService_DHKeyExchange(t *testing.T) {
	authService := service.NewServerAuthService(zap.NewExample(), mocks.NewCryptoInterface(t), mocks.NewAuthStateInterface(t))

	clientKey, err := ecdh.P256().GenerateKey(rand.Reader)
	if err != nil {
		t.Error(err)
	}

	sequence := uint32(0)
	send := protocol.CreateAuthPacket(protocol.PACKET_TYPE_DH_KEY_EXCH, "", sequence)
	send.DhKeyExch = &protocol.DHKeyExch{}
	marshalledPublicKey, err := x509.MarshalPKIXPublicKey(clientKey.PublicKey())
	if err != nil {
		t.Error(err)
	}
	send.DhKeyExch.ClientMaterial = base64.StdEncoding.EncodeToString(marshalledPublicKey)

	response, err := authService.DHKeyExchange(send)
	if err != nil {
		t.Error(err)
	}

	serverMaterial, err := base64.StdEncoding.DecodeString(response.DhKeyExch.GetServerMaterial())
	if err != nil {
		t.Error(err)
	}
	serverPublicKey, err := x509.ParsePKIXPublicKey(serverMaterial)
	if err != nil {
		t.Error(err)
	}
	serverECDH, err := (serverPublicKey.(*ecdsa.PublicKey)).ECDH()
	if err != nil {
		t.Error(err)
	}

	secret, err := clientKey.ECDH(serverECDH)
	if err != nil {
		t.Error(err)
	}
	fmt.Println("secret", secret)

}

func TestAuthService_ChallengeRequest(t *testing.T) {
	// arrange
	repo := mocks.NewAuthStateInterface(t)
	authService := service.NewServerAuthService(zap.NewExample(), mocks.NewCryptoInterface(t), mocks.NewAuthStateInterface(t))
	
	key := []byte("abcdefghijklmnop")
	encodedKey := base64.StdEncoding.EncodeToString(key)
	repo.On("GetUserKey", "username").Return(encodedKey, nil)
	
	// act
	challenge, solnHash, err := authService.ChallengeRequest("username")
	if err != nil {
		t.Error(err)
	}
	fmt.Println(challenge, solnHash)

	// assert
}

func TestAuthService_ChallengeSolution(t *testing.T) {
	// arrange
	repo := mocks.NewAuthStateInterface(t)
	authService := service.NewServerAuthService(zap.NewExample(), mocks.NewCryptoInterface(t), mocks.NewAuthStateInterface(t))

	key := []byte("abcdefghijklmnop")
	encodedKey := base64.StdEncoding.EncodeToString(key)
	repo.On("GetUserKey", "username").Return(encodedKey, nil)
	
	// act
	challenge, solnHash, err := authService.ChallengeRequest("username")
	if err != nil {
		t.Error(err)
	}
	fmt.Println(challenge, solnHash)

	// assert
}
