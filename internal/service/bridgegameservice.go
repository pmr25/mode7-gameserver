//go:build bridge

package service

import "go.uber.org/zap"

type BridgeGameService struct {
	log *zap.Logger
}

func NewBridgeGameService(log *zap.Logger) *BridgeGameService {
	return &BridgeGameService{
		log: log,
	}
}
