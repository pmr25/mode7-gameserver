//go:build bridge

package handler

import (
	"gitlab.com/pmr25/mode7-gameserver/internal/service"
	"go.uber.org/zap"
)

type BridgeGameHandler struct {
	log     *zap.Logger
	service *service.BridgeGameService
}

func NewBridgeGameHandler(log *zap.Logger, service *service.BridgeGameService) *BridgeGameHandler {
	return &BridgeGameHandler{
		log:     log,
		service: service,
	}
}
