//go:build bridge

package handler

import (
	"encoding/base64"
	"net"

	"gitlab.com/pmr25/mode7-gameserver/internal/service"
	"gitlab.com/pmr25/mode7-gameserver/pkg/protocol"
	"go.uber.org/zap"
	"google.golang.org/protobuf/proto"
)

type BridgeAuthHandler struct {
	service *service.BridgeAuthService
	log     *zap.Logger
}

func NewBridgeAuthHandler(service *service.BridgeAuthService, log *zap.Logger) *BridgeAuthHandler {
	return &BridgeAuthHandler{
		service: service,
		log:     log,
	}
}

func (h *BridgeAuthHandler) Handle(packetStr string) (string, error) {
	// decode request
	packetBytes, err := base64.StdEncoding.DecodeString(packetStr)
	if err != nil {
		return err.Error(), err
	}

	request := protocol.NewAuthPacketEmpty()
	if err := proto.Unmarshal(packetBytes, request); err != nil {
		return err.Error(), err
	}

	// process request
	response, err := h.processRequest(request)
	if err != nil {
		return err.Error(), err
	}

	// encode and send response
	responseBytes, err := proto.Marshal(response)
	if err != nil {
		return err.Error(), err
	}

	return base64.StdEncoding.EncodeToString(responseBytes), nil
}

func (h *BridgeAuthHandler) processRequest(request *protocol.AuthPacket) (*protocol.AuthPacket, error) {
	// connect to auth server
	tcpAddr, err := net.ResolveTCPAddr("tcp", "localhost:26290")
	if err != nil {
		return nil, err
	}
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		return nil, err
	}

	// send request to server
	requestBytes, err := proto.Marshal(request)
	if err != nil {
		panic(err)
	}
	_, err = conn.Write(requestBytes)
	if err != nil {
		return nil, err
	}

	// read response from server
	responseBytes := make([]byte, 1024)
	numRead, err := conn.Read(responseBytes)

	response := &protocol.AuthPacket{}
	if err := proto.Unmarshal(responseBytes[:numRead], response); err != nil {
		return nil, err
	}

	return response, nil
}
