//go:build server

package handler

import (
	"net"

	"gitlab.com/pmr25/mode7-gameserver/internal/service"
	"gitlab.com/pmr25/mode7-gameserver/pkg/protocol"
	"go.uber.org/zap"
	"google.golang.org/protobuf/proto"
)

type ServerGameHandler struct {
	log     *zap.Logger
	service *service.ServerGameService
}

func NewServerGameHandler(log *zap.Logger, service *service.ServerGameService) *ServerGameHandler {
	return &ServerGameHandler{
		log:     log,
		service: service,
	}
}

func (h *ServerGameHandler) Handle(addr *net.UDPAddr, data []byte) ([]byte, error) {
	request, err := h.decodePacket(data)
	if err != nil {
		h.log.Sugar().Errorf("decode error: %v", err)
		return []byte("error"), err
	}

	response, err := h.processPacket(request, addr)
	if err != nil {
		h.log.Sugar().Errorf("process error: %v", err)
		return nil, err
	}

	if response != nil {
		return h.encodePacket(response)
	}

	return nil, nil
}

func (h *ServerGameHandler) GetClients() []*net.UDPAddr {
	return h.service.GetClients()
}

func (h *ServerGameHandler) GetState() *protocol.GamePacket {
	return h.service.GetState()
}

func (h *ServerGameHandler) processPacket(packet *protocol.GamePacket, addr *net.UDPAddr) (*protocol.GamePacket, error) {
	action := packet.Packet.PacketType
	if action == protocol.PACKET_TYPE_JOIN_GAME {
		return h.service.RegisterClient(packet, addr)
	} else if action == protocol.PACKET_TYPE_QUIT_GAME {
		return nil, h.service.RemoveClient(packet)
	} else {
		return nil, h.service.ProcessPacket(packet, addr)
	}
}

func (h *ServerGameHandler) decodePacket(data []byte) (*protocol.GamePacket, error) {
	packet := &protocol.GamePacket{}
	if err := proto.Unmarshal(data, packet); err != nil {
		return nil, err
	}
	return packet, nil
}

func (h *ServerGameHandler) encodePacket(packet *protocol.GamePacket) ([]byte, error) {
	data, err := proto.Marshal(packet)
	if err != nil {
		return nil, err
	}
	return data, nil
}
