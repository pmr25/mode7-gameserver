//go:build server

package handler

import (
	"errors"

	"gitlab.com/pmr25/mode7-gameserver/internal/service"
	"gitlab.com/pmr25/mode7-gameserver/pkg/protocol"
	"go.uber.org/zap"
	"google.golang.org/protobuf/proto"
)

type ServerAuthHandler struct {
	service *service.ServerAuthService
	log     *zap.Logger
}

func (h *ServerAuthHandler) decodePacket(data []byte) (*protocol.AuthPacket, error) {
	packet := protocol.NewAuthPacketEmpty()
	if err := proto.Unmarshal(data, packet); err != nil {
		return nil, err
	}
	return packet, nil
}

func (h *ServerAuthHandler) encodePacket(packet *protocol.AuthPacket) ([]byte, error) {
	data, err := proto.Marshal(packet)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (h *ServerAuthHandler) processRequest(request *protocol.AuthPacket) (*protocol.AuthPacket, error) {
	if request.Packet.PacketType == protocol.PACKET_TYPE_TOKEN_REQUEST {
		return h.service.TokenRequest(request)
	} else if request.Packet.PacketType == protocol.PACKET_TYPE_DH_KEY_EXCH {
		return h.service.DHKeyExchange(request)
	} else if request.Packet.PacketType == protocol.PACKET_TYPE_NOTICE {
		if request.GetContent() == "health" {
			return h.service.Health(request)
		}
	}

	return nil, errors.New("unknown request")
}

func (h *ServerAuthHandler) Handle(data []byte) ([]byte, error) {
	request, err := h.decodePacket(data)
	if err != nil {
		return nil, err
	}

	response, err := h.processRequest(request)
	if err != nil {
		return nil, err
	}

	output, err := h.encodePacket(response)
	if err != nil {
		return nil, err
	}

	return output, nil
}

func NewServerAuthHandler(service *service.ServerAuthService, log *zap.Logger) *ServerAuthHandler {
	return &ServerAuthHandler{
		service: service,
		log:     log,
	}
}
