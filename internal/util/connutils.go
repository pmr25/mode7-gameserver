package util

import (
	"errors"
	"net"

	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/reflect/protoreflect"
)

func ReadPacketWithBuffer(conn *net.UDPConn, buffer []byte, message protoreflect.ProtoMessage) error {
	numRead, _, err := conn.ReadFromUDP(buffer)
	if err != nil {
		return err
	}
	if numRead < 0 {
		return nil
	}

	err = proto.Unmarshal(buffer[:numRead], message)
	if err != nil {
		return err
	}

	return nil
}

func ReadPacket(conn *net.UDPConn, message protoreflect.ProtoMessage) error {
	return ReadPacketWithBuffer(conn, make([]byte, 1024), message)
}

func WritePacket(conn *net.UDPConn, message protoreflect.ProtoMessage) error {
	payload, err := proto.Marshal(message)
	if err != nil {
		return err
	}

	numWritten, err := conn.Write(payload)
	if err != nil {
		return err
	}
	if numWritten <= 0 {
		return errors.New("wrote no bytes")
	}

	return nil
}
