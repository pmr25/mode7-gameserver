//go:build bridge && linux

package util

import (
	"bufio"
	"os"
	"strings"
	"syscall"
)

type Pipe struct {
	path       string
	Input      chan string
	Output     chan string
	inputPipe  *os.File
	outputPipe *os.File
}

func NewPipe(path string) *Pipe {
	return &Pipe{
		path:       path,
		Input:      make(chan string),
		Output:     make(chan string),
	}
}

func (p *Pipe) OpenAndRun() {
	defer p.cleanup()

	go p.readerLoop()
	go p.writerLoop()
}

func (p *Pipe) cleanup() {
}

func (p *Pipe) writerLoop() {
	outputPath := p.path + "_output"
	syscall.Mkfifo(outputPath, 0640)
	outputPipe, err := os.OpenFile(outputPath, os.O_RDWR, 0640)
	if err != nil {
		return
	}
	defer outputPipe.Close()

	for line := range p.Output {
		_, err := outputPipe.WriteString(line + "\n")
		if err != nil {
			panic(err)
		}
	}
}

func (p *Pipe) readerLoop() {
	inputPath := p.path + "_input"
	syscall.Mkfifo(inputPath, 0640)
	inputPipe, err := os.OpenFile(inputPath, os.O_RDWR, 0640)
	if err != nil {
		return
	}
	defer inputPipe.Close()

	reader := bufio.NewReader(inputPipe)

	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			break
		}
		packet := strings.TrimRight(line, "\n")
		p.Input <- packet
	}
}
