package authclient

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"

	"gitlab.com/pmr25/mode7-gameserver/pkg/protocol"
	"google.golang.org/protobuf/proto"
)

func showMenu() {
	fmt.Println("Choose an option:\n\t0) Quit\n\t1) Get token")
	fmt.Print("=> ")
}

func getUserSelection(reader *bufio.Reader) (int, error) {
	line, _, err := reader.ReadLine()
	if err != nil {
		return -1, err
	}

	selection, err := strconv.Atoi(string(line))
	if err != nil {
		return -1, nil
	}

	return selection, nil
}

func getAuthToken(conn *net.TCPConn) {
	packet := protocol.CreateAuthPacket(protocol.PACKET_TYPE_TOKEN_REQUEST, "hello world", 1)

	data, err := proto.Marshal(packet)
	if err != nil {
		log.Fatal(err)
	}
	_, err = conn.Write(data)
	if err != nil {
		log.Fatal(err)
	}

	response := make([]byte, 1024)
	numRead, err := conn.Read(response)
	if err != nil {
		log.Fatal(err)
	}
	if numRead < 1 {
		log.Fatal("numread=", numRead)
	}

	responsePacket := &protocol.AuthPacket{}
	err = proto.Unmarshal(response[:numRead], responsePacket)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("response=%v\n", responsePacket.Content)
}

func executeOption(option int, conn *net.TCPConn) {
	if option < 0 {
		fmt.Println("bad format")
	} else if option == 0 {
		os.Exit(0)
	} else if option == 1 {
		getAuthToken(conn)
	} else {
		fmt.Println("unknown option")
	}
}

func Run() {
	tcpAddr, err := net.ResolveTCPAddr("tcp", "localhost:26290")
	if err != nil {
		log.Fatal(err)
	}

	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		log.Fatal(err)
	}

	reader := bufio.NewReader(os.Stdin)
	for {
		showMenu()
		option, err := getUserSelection(reader)
		if err != nil {
			log.Fatal(err)
		}
		executeOption(option, conn)
	}
}
