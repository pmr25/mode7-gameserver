package gameclient

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	"github.com/google/uuid"
	"gitlab.com/pmr25/mode7-gameserver/internal/util"
	"gitlab.com/pmr25/mode7-gameserver/pkg/protocol"
)

var conn *net.UDPConn = nil
var id string = uuid.NewString()

func showMenu() {
	fmt.Println("Choose an option:\n\t0) Quit\n\t1) Listener\n\t2) Listen and Send")
	fmt.Print("=> ")
}

func getUserSelection(reader *bufio.Reader) (int, error) {
	line, _, err := reader.ReadLine()
	if err != nil {
		return -1, err
	}

	selection, err := strconv.Atoi(string(line))
	if err != nil {
		return -1, nil
	}

	return selection, nil
}

func executeOption(option int, conn *net.UDPConn) {
	if option < 0 {
		fmt.Println("bad format")
	} else if option == 0 {
		os.Exit(0)
	} else if option == 1 {
		listenOnly(conn)
	} else if option == 2 {
		listenSend(conn)
	} else {
		fmt.Println("unknown option")
	}
}

func Run() {
	SetupSignals()

	sock, err := net.ResolveUDPAddr("udp4", ":10262")
	if err != nil {
		log.Fatal(err)
	}
	conn, err = net.DialUDP("udp4", nil, sock)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	reader := bufio.NewReader(os.Stdin)
	for {
		showMenu()
		option, err := getUserSelection(reader)
		if err != nil {
			log.Fatal(err)
		}
		executeOption(option, conn)
	}
}

func listenOnly(conn *net.UDPConn) {
	buffer := make([]byte, 1024)

	packet := protocol.NewGamePacket(protocol.PACKET_TYPE_JOIN_GAME)
	packet.Packet.Token.Id = id
	if err := util.WritePacket(conn, packet); err != nil {
		panic(err)
	}
	response := &protocol.GamePacket{}
	if err := util.ReadPacketWithBuffer(conn, buffer, response); err != nil {
		panic(err)
	}

	for {
		numRead, _, err := conn.ReadFromUDP(buffer)
		if err != nil {
			log.Fatal(err)
		}
		if numRead < 0 {
			continue
		}

		fmt.Println(string(buffer[:numRead]))
	}
}

func listenSend(conn *net.UDPConn) {
	buffer := make([]byte, 1024)

	packet := protocol.NewGamePacket(protocol.PACKET_TYPE_JOIN_GAME)
	packet.Packet.Token.Id = id
	if err := util.WritePacket(conn, packet); err != nil {
		panic(err)
	}
	response := &protocol.GamePacket{}
	if err := util.ReadPacketWithBuffer(conn, buffer, response); err != nil {
		panic(err)
	}

	state := protocol.NewGamePacket(protocol.PACKET_TYPE_NOTICE)
	state.VehicleStates = make([]*protocol.VehicleState, 1)
	state.Packet.Token.Id = id
	state.VehicleStates[0] = &protocol.VehicleState{
		Id:        id,
		PositionX: 0.1,
		PositionY: 0.3,
	}
	if err := util.WritePacket(conn, state); err != nil {
		panic(err)
	}

	for {
		numRead, _, err := conn.ReadFromUDP(buffer)
		if err != nil {
			log.Fatal(err)
		}
		if numRead < 0 {
			continue
		}

		message := &protocol.GamePacket{}

		if err := util.ReadPacketWithBuffer(conn, buffer, message); err != nil {
			fmt.Printf("error: cannot read packet for reason %v\n", err)
			continue
		}

		if message.Packet.PacketType != protocol.PACKET_TYPE_SERVER_STATE {
			fmt.Printf("ignoring packet of type %v\n", message.Packet.PacketType)
			continue
		}

		fmt.Printf("num vehicle states = %d\n", len(message.VehicleStates))
		for _, veh := range message.VehicleStates {
			fmt.Printf("\t[%s](%v,%v)\n", veh.Id, veh.PositionX, veh.PositionY)
		}
	}
}

func SendQuitPacket() {
	packet := protocol.NewGamePacket(protocol.PACKET_TYPE_QUIT_GAME)
	packet.Packet.Token.Id = id
	if err := util.WritePacket(conn, packet); err != nil {
		panic(err)
	}
}

func SetupSignals() {
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		fmt.Println("leaving and shutting down...")
		SendQuitPacket()
		os.Exit(0)
	}()
}
