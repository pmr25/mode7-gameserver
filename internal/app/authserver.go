//go:build server

package app

import (
	"context"
	"net"

	"go.uber.org/fx"
	"go.uber.org/zap"

	"gitlab.com/pmr25/mode7-gameserver/internal/handler"
)

type AuthServer struct {
	handler *handler.ServerAuthHandler
	log     *zap.Logger
}

func NewAuthServer(lc fx.Lifecycle, handler *handler.ServerAuthHandler, log *zap.Logger) *AuthServer {
	server := &AuthServer{
		handler: handler,
		log:     log,
	}

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			go server.Serve()
			return nil
		},
		OnStop: func(ctx context.Context) error {
			return nil
		},
	})

	return server
}

func (s *AuthServer) handle(conn net.Conn) {
	buffer := make([]byte, 1024)
	for {
		numRead, err := conn.Read(buffer)
		if err != nil {
			s.log.Sugar().Errorf("read error: %v", err)
			break
		}
		if numRead <= 0 {
			break
		}

		response, err := s.handler.Handle(buffer[:numRead])
		if err != nil {
			s.log.Sugar().Errorf("handle error: %v", err)
			break
		}

		numWritten, err := conn.Write(response)
		if err != nil {
			s.log.Sugar().Errorf("write error: %v", err)
			break
		}
		if numWritten <= 0 {
			break
		}
	}
}

func (s *AuthServer) Serve() error {
	sock, err := net.Listen("tcp", ":26290")
	if err != nil {
		s.log.Sugar().Errorf("sock error: %v", err)
		return err
	}
	defer sock.Close()

	for {
		conn, err := sock.Accept()
		if err != nil {
			s.log.Sugar().Errorf("connect error: %v", err)
			continue
		}

		go s.handle(conn)
	}
}
