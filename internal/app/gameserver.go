//go:build server

package app

import (
	"context"
	"net"
	"time"

	"gitlab.com/pmr25/mode7-gameserver/internal/handler"
	"go.uber.org/fx"
	"go.uber.org/zap"
	"google.golang.org/protobuf/proto"
)

var SERVER_TICK time.Duration = 50 * time.Millisecond

type GameServer struct {
	conn    *net.UDPConn
	handler *handler.ServerGameHandler
	log     *zap.Logger
}

func NewGameServer(lc fx.Lifecycle, handler *handler.ServerGameHandler, log *zap.Logger) *GameServer {
	server := &GameServer{
		conn:    nil,
		handler: handler,
		log:     log,
	}

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			go server.Serve()
			return nil
		},
		OnStop: func(ctx context.Context) error {
			return nil
		},
	})

	return server
}

func (s *GameServer) Serve() error {
	sock, err := net.ResolveUDPAddr("udp4", ":10262")
	if err != nil {
		s.log.Sugar().Error(err)
		return err
	}

	s.conn, err = net.ListenUDP("udp4", sock)
	if err != nil {
		return err
	}
	defer s.conn.Close()

	s.log.Sugar().Info("starting distributor")
	go s.distribute()

	s.log.Sugar().Info("server is running")
	for {
		buffer := make([]byte, 1024)
		numRead, addr, err := s.conn.ReadFromUDP(buffer)
		if err != nil {
			s.log.Sugar().Error(err)
			return err
		}
		s.log.Sugar().Infof("got %d bytes from %v", numRead, addr.IP)
		go s.handle(addr, buffer[:numRead])
	}
}

func (s *GameServer) distribute() {
	s.log.Sugar().Info("distributor is running")

	timer := time.Tick(SERVER_TICK)
	for range timer {
		// get clients to broadcast to
		clients := s.handler.GetClients()
		if clients == nil {
			continue
		}

		data, err := proto.Marshal(s.handler.GetState())
		if err != nil {
			s.log.Sugar().Warnf("error sending server state: %v", err)
			continue
		}

		for _, addr := range clients {
			go s.Send(addr, data)
		}
	}
}

func (s *GameServer) handle(addr *net.UDPAddr, request []byte) {
	response, err := s.handler.Handle(addr, request)
	if err != nil {
		s.log.Sugar().Errorf("handle error: %v", err)
	}
	if len(response) < 1 {
		s.log.Sugar().Debug("empty response")
		return
	}
	s.log.Sugar().Debugf("handle response send %d bytes", len(response))
	s.Send(addr, response)
}

func (s *GameServer) Send(addr *net.UDPAddr, data []byte) {
	if data == nil {
		s.log.Sugar().Debug("cannot send nil data")
		return
	}

	if addr == nil {
		s.log.Sugar().Panic("nil addr")
	}

	_, err := s.conn.WriteToUDP(data, addr)
	if err != nil {
		s.log.Sugar().Errorf("write error: %v", err)
		return
	}
}
