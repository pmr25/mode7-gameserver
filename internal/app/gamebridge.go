//go:build bridge

package app

import (
	"go.uber.org/fx"
	"go.uber.org/zap"
)

type GameBridge struct {
	log *zap.Logger
}

func NewGameBridge(lc fx.Lifecycle, log *zap.Logger) *GameBridge {
	bridge := &GameBridge{
		log: log,
	}

	return bridge
}
