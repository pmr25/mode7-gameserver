//go:build bridge

package app

import (
	"context"

	"go.uber.org/fx"
	"go.uber.org/zap"

	"gitlab.com/pmr25/mode7-gameserver/internal/handler"
	"gitlab.com/pmr25/mode7-gameserver/internal/util"
)

type AuthBridge struct {
	handler *handler.BridgeAuthHandler
	log     *zap.Logger
}

func NewAuthBridge(lc fx.Lifecycle, handler *handler.BridgeAuthHandler, log *zap.Logger) *AuthBridge {
	bridge := &AuthBridge{
		handler: handler,
		log:     log,
	}

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			go bridge.Serve()
			return nil
		},
		OnStop: func(ctx context.Context) error {
			return nil
		},
	})

	return bridge
}

func (b *AuthBridge) Serve() error {
	pipe := util.NewPipe("/tmp/authbridgep")
	pipe.OpenAndRun()

	var response string
	var err error

	for request := range pipe.Input {
		response, err = b.handler.Handle(request)
		if err != nil {
			b.log.Sugar().Error(err)
		}

		pipe.Output <- response
	}

	return nil
}
