//go:build server

package main

import (
	"flag"
	"fmt"

	"gitlab.com/pmr25/mode7-gameserver/internal/app"
	authclient "gitlab.com/pmr25/mode7-gameserver/internal/client/auth"
	gameclient "gitlab.com/pmr25/mode7-gameserver/internal/client/game"
	"gitlab.com/pmr25/mode7-gameserver/internal/handler"
	"gitlab.com/pmr25/mode7-gameserver/internal/service"
	"gitlab.com/pmr25/mode7-gameserver/internal/state"
	"go.uber.org/fx"
	"go.uber.org/zap"
)

var appFlag = flag.String("app", "", "the app to run")

func main() {
	flag.Parse()

	deps := fx.Provide(
		zap.NewExample,

		service.NewServerAuthService,
		service.NewServerGameService,

		handler.NewServerAuthHandler,
		handler.NewServerGameHandler,

		state.NewGameState,
		state.NewAuthState,
		fx.Annotate(state.NewAuthState, fx.As(new(state.AuthStateInterface))),
		fx.Annotate(service.NewCryptoService, fx.As(new(service.CryptoInterface))),
	)
	var apps fx.Option = nil

	if *appFlag == "authserver" {
		apps = fx.Invoke(app.NewAuthServer)
	} else if *appFlag == "authclient" {
		authclient.Run()
		return
	} else if *appFlag == "gameserver" {
		apps = fx.Invoke(
			app.NewGameServer,
		)
	} else if *appFlag == "gameclient" {
		gameclient.Run()
	} else {
		fmt.Println("unknown app")
	}

	if apps == nil {
		return
	}

	fx.New(
		deps,
		apps,
	).Run()
}
