package protocol_test

import (
	"reflect"
	"testing"

	"gitlab.com/pmr25/mode7-gameserver/pkg/protocol"
	"google.golang.org/protobuf/proto"
)

func TestAuthPacket_SerializeDeserialize(t *testing.T) {
	send := &protocol.AuthPacket{}
	send.Content = "hello world"

	buffer, err := proto.Marshal(send)
	if err != nil {
		t.Error(err)
	}

	recv := &protocol.AuthPacket{}
	if err := proto.Unmarshal(buffer, recv); err != nil {
		t.Error(err)
	}

	if recv.Content != send.Content {
		t.Error("received != sent")
	}
}

func TestAuthPacket_Full_SerializeDeserialize(t *testing.T) {
	send := protocol.CreateAuthPacket(protocol.PACKET_TYPE_TOKEN_REQUEST, "hello world", 1)

	buffer, err := proto.Marshal(send)
	if err != nil {
		t.Error(err)
	}

	recv := &protocol.AuthPacket{}
	if err := proto.Unmarshal(buffer, recv); err != nil {
		t.Error(err)
	}

	if !reflect.DeepEqual(recv.Request, send.Request) {
		t.Error("Request received != sent")
	}

	if !reflect.DeepEqual(recv.Content, send.Content) {
		t.Error("Content received != sent")
	}
}
