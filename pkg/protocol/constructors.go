package protocol

import (
	"time"

	"github.com/google/uuid"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func CreateEmptyToken() *Token {
	token := &Token{}
	token.Material = "secret"
	token.Signature = &Signature{}
	token.Id = uuid.NewString()
	token.GrantedAt = timestamppb.Now()
	token.ExpiresAt = timestamppb.New(time.Now().Add(1 * time.Hour))
	return token
}

func CreatePacket(packet_type PACKET_TYPE, sequence uint32) *Packet {
	packet := &Packet{}
	packet.PacketType = packet_type
	packet.Timestamp = timestamppb.Now()
	packet.Token = CreateEmptyToken()
	packet.Sequence = sequence
	return packet
}

func NewPacketEmpty() *Packet {
	packet := &Packet{}
	packet.Token = CreateEmptyToken()
	return packet
}

func CreateAuthPacket(packet_type PACKET_TYPE, content string, sequence uint32) *AuthPacket {
	authPacket := &AuthPacket{}
	authPacket.Packet = CreatePacket(packet_type, sequence)
	authPacket.Content = content
	return authPacket
}

func NewAuthPacketEmpty() *AuthPacket {
	authPacket := &AuthPacket{}
	authPacket.Packet = NewPacketEmpty()
	return authPacket
}

func NewGamePacketEmpty() *GamePacket {
	gamePacket := &GamePacket{}
	gamePacket.Packet = NewPacketEmpty()
	gamePacket.RaceState = &RaceControlState{}
	return gamePacket
}

func NewGamePacket(packetType PACKET_TYPE) *GamePacket {
	gamePacket := &GamePacket{}
	gamePacket.Packet = CreatePacket(packetType, 0)
	return gamePacket
}
