@echo off

mkdir 3rdparty/protobuf
cd 3rdparty/protobuf
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
wget https://github.com/protocolbuffers/protobuf/releases/download/v22.2/protoc-22.2-win64.zip
unzip protoc-22.2-win64.zip
del protoc-22.2-win64.zip