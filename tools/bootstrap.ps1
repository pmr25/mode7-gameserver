go install google.golang.org/protobuf/cmd/protoc-gen-go@latest

mkdir -p 3rdparty/protobuf
cd 3rdparty/protobuf
wget https://github.com/protocolbuffers/protobuf/releases/download/v22.2/protoc-22.2-win64.zip -OutFile "protobuf.zip"
Expand-Archive protobuf.zip -DestinationPath ./
del protobuf.zip
cd ../..