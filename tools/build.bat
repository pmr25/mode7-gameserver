@echo off

3rdparty\protobuf\bin\protoc -I=3rdparty\protobuf\include -I=assets/protobuf --go_out=. assets\protobuf\*.proto
go mod tidy
go build