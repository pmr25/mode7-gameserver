// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: packet_type.proto

#ifndef GOOGLE_PROTOBUF_INCLUDED_packet_5ftype_2eproto
#define GOOGLE_PROTOBUF_INCLUDED_packet_5ftype_2eproto

#include <limits>
#include <string>

#include <google/protobuf/port_def.inc>
#if PROTOBUF_VERSION < 3021000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers. Please update
#error your headers.
#endif
#if 3021012 < PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers. Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/port_undef.inc>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/metadata_lite.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/repeated_field.h>  // IWYU pragma: export
#include <google/protobuf/extension_set.h>  // IWYU pragma: export
#include <google/protobuf/generated_enum_reflection.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
#define PROTOBUF_INTERNAL_EXPORT_packet_5ftype_2eproto
PROTOBUF_NAMESPACE_OPEN
namespace internal {
class AnyMetadata;
}  // namespace internal
PROTOBUF_NAMESPACE_CLOSE

// Internal implementation detail -- do not use these members.
struct TableStruct_packet_5ftype_2eproto {
  static const uint32_t offsets[];
};
extern const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_packet_5ftype_2eproto;
PROTOBUF_NAMESPACE_OPEN
PROTOBUF_NAMESPACE_CLOSE
namespace gameserver {

enum PACKET_TYPE : int {
  NOTICE = 0,
  AUTH = 1,
  DH_KEY_EXCH = 2,
  TOKEN_REQUEST = 3,
  TOKEN_ISSUE = 4,
  TOKEN_RENEW = 5,
  PUBKEY_REQUEST = 6,
  PUBKEY_RESONSE = 7,
  JOIN_GAME = 8,
  QUIT_GAME = 9,
  SERVER_STATE = 10,
  PACKET_TYPE_INT_MIN_SENTINEL_DO_NOT_USE_ = std::numeric_limits<int32_t>::min(),
  PACKET_TYPE_INT_MAX_SENTINEL_DO_NOT_USE_ = std::numeric_limits<int32_t>::max()
};
bool PACKET_TYPE_IsValid(int value);
constexpr PACKET_TYPE PACKET_TYPE_MIN = NOTICE;
constexpr PACKET_TYPE PACKET_TYPE_MAX = SERVER_STATE;
constexpr int PACKET_TYPE_ARRAYSIZE = PACKET_TYPE_MAX + 1;

const ::PROTOBUF_NAMESPACE_ID::EnumDescriptor* PACKET_TYPE_descriptor();
template<typename T>
inline const std::string& PACKET_TYPE_Name(T enum_t_value) {
  static_assert(::std::is_same<T, PACKET_TYPE>::value ||
    ::std::is_integral<T>::value,
    "Incorrect type passed to function PACKET_TYPE_Name.");
  return ::PROTOBUF_NAMESPACE_ID::internal::NameOfEnum(
    PACKET_TYPE_descriptor(), enum_t_value);
}
inline bool PACKET_TYPE_Parse(
    ::PROTOBUF_NAMESPACE_ID::ConstStringParam name, PACKET_TYPE* value) {
  return ::PROTOBUF_NAMESPACE_ID::internal::ParseNamedEnum<PACKET_TYPE>(
    PACKET_TYPE_descriptor(), name, value);
}
// ===================================================================


// ===================================================================


// ===================================================================

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif  // __GNUC__
#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif  // __GNUC__

// @@protoc_insertion_point(namespace_scope)

}  // namespace gameserver

PROTOBUF_NAMESPACE_OPEN

template <> struct is_proto_enum< ::gameserver::PACKET_TYPE> : ::std::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::gameserver::PACKET_TYPE>() {
  return ::gameserver::PACKET_TYPE_descriptor();
}

PROTOBUF_NAMESPACE_CLOSE

// @@protoc_insertion_point(global_scope)

#include <google/protobuf/port_undef.inc>
#endif  // GOOGLE_PROTOBUF_INCLUDED_GOOGLE_PROTOBUF_INCLUDED_packet_5ftype_2eproto
