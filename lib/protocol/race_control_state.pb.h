// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: race_control_state.proto

#ifndef GOOGLE_PROTOBUF_INCLUDED_race_5fcontrol_5fstate_2eproto
#define GOOGLE_PROTOBUF_INCLUDED_race_5fcontrol_5fstate_2eproto

#include <limits>
#include <string>

#include <google/protobuf/port_def.inc>
#if PROTOBUF_VERSION < 3021000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers. Please update
#error your headers.
#endif
#if 3021012 < PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers. Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/port_undef.inc>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/metadata_lite.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>  // IWYU pragma: export
#include <google/protobuf/extension_set.h>  // IWYU pragma: export
#include <google/protobuf/unknown_field_set.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
#define PROTOBUF_INTERNAL_EXPORT_race_5fcontrol_5fstate_2eproto
PROTOBUF_NAMESPACE_OPEN
namespace internal {
class AnyMetadata;
}  // namespace internal
PROTOBUF_NAMESPACE_CLOSE

// Internal implementation detail -- do not use these members.
struct TableStruct_race_5fcontrol_5fstate_2eproto {
  static const uint32_t offsets[];
};
extern const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_race_5fcontrol_5fstate_2eproto;
namespace gameserver {
class RaceControlState;
struct RaceControlStateDefaultTypeInternal;
extern RaceControlStateDefaultTypeInternal _RaceControlState_default_instance_;
}  // namespace gameserver
PROTOBUF_NAMESPACE_OPEN
template<> ::gameserver::RaceControlState* Arena::CreateMaybeMessage<::gameserver::RaceControlState>(Arena*);
PROTOBUF_NAMESPACE_CLOSE
namespace gameserver {

// ===================================================================

class RaceControlState final :
    public ::PROTOBUF_NAMESPACE_ID::Message /* @@protoc_insertion_point(class_definition:gameserver.RaceControlState) */ {
 public:
  inline RaceControlState() : RaceControlState(nullptr) {}
  ~RaceControlState() override;
  explicit PROTOBUF_CONSTEXPR RaceControlState(::PROTOBUF_NAMESPACE_ID::internal::ConstantInitialized);

  RaceControlState(const RaceControlState& from);
  RaceControlState(RaceControlState&& from) noexcept
    : RaceControlState() {
    *this = ::std::move(from);
  }

  inline RaceControlState& operator=(const RaceControlState& from) {
    CopyFrom(from);
    return *this;
  }
  inline RaceControlState& operator=(RaceControlState&& from) noexcept {
    if (this == &from) return *this;
    if (GetOwningArena() == from.GetOwningArena()
  #ifdef PROTOBUF_FORCE_COPY_IN_MOVE
        && GetOwningArena() != nullptr
  #endif  // !PROTOBUF_FORCE_COPY_IN_MOVE
    ) {
      InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }

  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* descriptor() {
    return GetDescriptor();
  }
  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* GetDescriptor() {
    return default_instance().GetMetadata().descriptor;
  }
  static const ::PROTOBUF_NAMESPACE_ID::Reflection* GetReflection() {
    return default_instance().GetMetadata().reflection;
  }
  static const RaceControlState& default_instance() {
    return *internal_default_instance();
  }
  static inline const RaceControlState* internal_default_instance() {
    return reinterpret_cast<const RaceControlState*>(
               &_RaceControlState_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    0;

  friend void swap(RaceControlState& a, RaceControlState& b) {
    a.Swap(&b);
  }
  inline void Swap(RaceControlState* other) {
    if (other == this) return;
  #ifdef PROTOBUF_FORCE_COPY_IN_SWAP
    if (GetOwningArena() != nullptr &&
        GetOwningArena() == other->GetOwningArena()) {
   #else  // PROTOBUF_FORCE_COPY_IN_SWAP
    if (GetOwningArena() == other->GetOwningArena()) {
  #endif  // !PROTOBUF_FORCE_COPY_IN_SWAP
      InternalSwap(other);
    } else {
      ::PROTOBUF_NAMESPACE_ID::internal::GenericSwap(this, other);
    }
  }
  void UnsafeArenaSwap(RaceControlState* other) {
    if (other == this) return;
    GOOGLE_DCHECK(GetOwningArena() == other->GetOwningArena());
    InternalSwap(other);
  }

  // implements Message ----------------------------------------------

  RaceControlState* New(::PROTOBUF_NAMESPACE_ID::Arena* arena = nullptr) const final {
    return CreateMaybeMessage<RaceControlState>(arena);
  }
  using ::PROTOBUF_NAMESPACE_ID::Message::CopyFrom;
  void CopyFrom(const RaceControlState& from);
  using ::PROTOBUF_NAMESPACE_ID::Message::MergeFrom;
  void MergeFrom( const RaceControlState& from) {
    RaceControlState::MergeImpl(*this, from);
  }
  private:
  static void MergeImpl(::PROTOBUF_NAMESPACE_ID::Message& to_msg, const ::PROTOBUF_NAMESPACE_ID::Message& from_msg);
  public:
  PROTOBUF_ATTRIBUTE_REINITIALIZES void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  const char* _InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) final;
  uint8_t* _InternalSerialize(
      uint8_t* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const final;
  int GetCachedSize() const final { return _impl_._cached_size_.Get(); }

  private:
  void SharedCtor(::PROTOBUF_NAMESPACE_ID::Arena* arena, bool is_message_owned);
  void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(RaceControlState* other);

  private:
  friend class ::PROTOBUF_NAMESPACE_ID::internal::AnyMetadata;
  static ::PROTOBUF_NAMESPACE_ID::StringPiece FullMessageName() {
    return "gameserver.RaceControlState";
  }
  protected:
  explicit RaceControlState(::PROTOBUF_NAMESPACE_ID::Arena* arena,
                       bool is_message_owned = false);
  public:

  static const ClassData _class_data_;
  const ::PROTOBUF_NAMESPACE_ID::Message::ClassData*GetClassData() const final;

  ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadata() const final;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  enum : int {
    kCountdownFieldNumber = 1,
    kNumRacersFieldNumber = 2,
  };
  // uint32 countdown = 1;
  void clear_countdown();
  uint32_t countdown() const;
  void set_countdown(uint32_t value);
  private:
  uint32_t _internal_countdown() const;
  void _internal_set_countdown(uint32_t value);
  public:

  // uint32 num_racers = 2;
  void clear_num_racers();
  uint32_t num_racers() const;
  void set_num_racers(uint32_t value);
  private:
  uint32_t _internal_num_racers() const;
  void _internal_set_num_racers(uint32_t value);
  public:

  // @@protoc_insertion_point(class_scope:gameserver.RaceControlState)
 private:
  class _Internal;

  template <typename T> friend class ::PROTOBUF_NAMESPACE_ID::Arena::InternalHelper;
  typedef void InternalArenaConstructable_;
  typedef void DestructorSkippable_;
  struct Impl_ {
    uint32_t countdown_;
    uint32_t num_racers_;
    mutable ::PROTOBUF_NAMESPACE_ID::internal::CachedSize _cached_size_;
  };
  union { Impl_ _impl_; };
  friend struct ::TableStruct_race_5fcontrol_5fstate_2eproto;
};
// ===================================================================


// ===================================================================

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif  // __GNUC__
// RaceControlState

// uint32 countdown = 1;
inline void RaceControlState::clear_countdown() {
  _impl_.countdown_ = 0u;
}
inline uint32_t RaceControlState::_internal_countdown() const {
  return _impl_.countdown_;
}
inline uint32_t RaceControlState::countdown() const {
  // @@protoc_insertion_point(field_get:gameserver.RaceControlState.countdown)
  return _internal_countdown();
}
inline void RaceControlState::_internal_set_countdown(uint32_t value) {
  
  _impl_.countdown_ = value;
}
inline void RaceControlState::set_countdown(uint32_t value) {
  _internal_set_countdown(value);
  // @@protoc_insertion_point(field_set:gameserver.RaceControlState.countdown)
}

// uint32 num_racers = 2;
inline void RaceControlState::clear_num_racers() {
  _impl_.num_racers_ = 0u;
}
inline uint32_t RaceControlState::_internal_num_racers() const {
  return _impl_.num_racers_;
}
inline uint32_t RaceControlState::num_racers() const {
  // @@protoc_insertion_point(field_get:gameserver.RaceControlState.num_racers)
  return _internal_num_racers();
}
inline void RaceControlState::_internal_set_num_racers(uint32_t value) {
  
  _impl_.num_racers_ = value;
}
inline void RaceControlState::set_num_racers(uint32_t value) {
  _internal_set_num_racers(value);
  // @@protoc_insertion_point(field_set:gameserver.RaceControlState.num_racers)
}

#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif  // __GNUC__

// @@protoc_insertion_point(namespace_scope)

}  // namespace gameserver

// @@protoc_insertion_point(global_scope)

#include <google/protobuf/port_undef.inc>
#endif  // GOOGLE_PROTOBUF_INCLUDED_GOOGLE_PROTOBUF_INCLUDED_race_5fcontrol_5fstate_2eproto
