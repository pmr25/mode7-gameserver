// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: packet.proto

#ifndef GOOGLE_PROTOBUF_INCLUDED_packet_2eproto
#define GOOGLE_PROTOBUF_INCLUDED_packet_2eproto

#include <limits>
#include <string>

#include <google/protobuf/port_def.inc>
#if PROTOBUF_VERSION < 3021000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers. Please update
#error your headers.
#endif
#if 3021012 < PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers. Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/port_undef.inc>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/metadata_lite.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>  // IWYU pragma: export
#include <google/protobuf/extension_set.h>  // IWYU pragma: export
#include <google/protobuf/unknown_field_set.h>
#include <google/protobuf/timestamp.pb.h>
#include "packet_type.pb.h"
#include "token.pb.h"
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
#define PROTOBUF_INTERNAL_EXPORT_packet_2eproto
PROTOBUF_NAMESPACE_OPEN
namespace internal {
class AnyMetadata;
}  // namespace internal
PROTOBUF_NAMESPACE_CLOSE

// Internal implementation detail -- do not use these members.
struct TableStruct_packet_2eproto {
  static const uint32_t offsets[];
};
extern const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_packet_2eproto;
namespace gameserver {
class Packet;
struct PacketDefaultTypeInternal;
extern PacketDefaultTypeInternal _Packet_default_instance_;
}  // namespace gameserver
PROTOBUF_NAMESPACE_OPEN
template<> ::gameserver::Packet* Arena::CreateMaybeMessage<::gameserver::Packet>(Arena*);
PROTOBUF_NAMESPACE_CLOSE
namespace gameserver {

// ===================================================================

class Packet final :
    public ::PROTOBUF_NAMESPACE_ID::Message /* @@protoc_insertion_point(class_definition:gameserver.Packet) */ {
 public:
  inline Packet() : Packet(nullptr) {}
  ~Packet() override;
  explicit PROTOBUF_CONSTEXPR Packet(::PROTOBUF_NAMESPACE_ID::internal::ConstantInitialized);

  Packet(const Packet& from);
  Packet(Packet&& from) noexcept
    : Packet() {
    *this = ::std::move(from);
  }

  inline Packet& operator=(const Packet& from) {
    CopyFrom(from);
    return *this;
  }
  inline Packet& operator=(Packet&& from) noexcept {
    if (this == &from) return *this;
    if (GetOwningArena() == from.GetOwningArena()
  #ifdef PROTOBUF_FORCE_COPY_IN_MOVE
        && GetOwningArena() != nullptr
  #endif  // !PROTOBUF_FORCE_COPY_IN_MOVE
    ) {
      InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }

  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* descriptor() {
    return GetDescriptor();
  }
  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* GetDescriptor() {
    return default_instance().GetMetadata().descriptor;
  }
  static const ::PROTOBUF_NAMESPACE_ID::Reflection* GetReflection() {
    return default_instance().GetMetadata().reflection;
  }
  static const Packet& default_instance() {
    return *internal_default_instance();
  }
  static inline const Packet* internal_default_instance() {
    return reinterpret_cast<const Packet*>(
               &_Packet_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    0;

  friend void swap(Packet& a, Packet& b) {
    a.Swap(&b);
  }
  inline void Swap(Packet* other) {
    if (other == this) return;
  #ifdef PROTOBUF_FORCE_COPY_IN_SWAP
    if (GetOwningArena() != nullptr &&
        GetOwningArena() == other->GetOwningArena()) {
   #else  // PROTOBUF_FORCE_COPY_IN_SWAP
    if (GetOwningArena() == other->GetOwningArena()) {
  #endif  // !PROTOBUF_FORCE_COPY_IN_SWAP
      InternalSwap(other);
    } else {
      ::PROTOBUF_NAMESPACE_ID::internal::GenericSwap(this, other);
    }
  }
  void UnsafeArenaSwap(Packet* other) {
    if (other == this) return;
    GOOGLE_DCHECK(GetOwningArena() == other->GetOwningArena());
    InternalSwap(other);
  }

  // implements Message ----------------------------------------------

  Packet* New(::PROTOBUF_NAMESPACE_ID::Arena* arena = nullptr) const final {
    return CreateMaybeMessage<Packet>(arena);
  }
  using ::PROTOBUF_NAMESPACE_ID::Message::CopyFrom;
  void CopyFrom(const Packet& from);
  using ::PROTOBUF_NAMESPACE_ID::Message::MergeFrom;
  void MergeFrom( const Packet& from) {
    Packet::MergeImpl(*this, from);
  }
  private:
  static void MergeImpl(::PROTOBUF_NAMESPACE_ID::Message& to_msg, const ::PROTOBUF_NAMESPACE_ID::Message& from_msg);
  public:
  PROTOBUF_ATTRIBUTE_REINITIALIZES void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  const char* _InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) final;
  uint8_t* _InternalSerialize(
      uint8_t* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const final;
  int GetCachedSize() const final { return _impl_._cached_size_.Get(); }

  private:
  void SharedCtor(::PROTOBUF_NAMESPACE_ID::Arena* arena, bool is_message_owned);
  void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(Packet* other);

  private:
  friend class ::PROTOBUF_NAMESPACE_ID::internal::AnyMetadata;
  static ::PROTOBUF_NAMESPACE_ID::StringPiece FullMessageName() {
    return "gameserver.Packet";
  }
  protected:
  explicit Packet(::PROTOBUF_NAMESPACE_ID::Arena* arena,
                       bool is_message_owned = false);
  public:

  static const ClassData _class_data_;
  const ::PROTOBUF_NAMESPACE_ID::Message::ClassData*GetClassData() const final;

  ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadata() const final;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  enum : int {
    kTimestampFieldNumber = 2,
    kTokenFieldNumber = 3,
    kPacketTypeFieldNumber = 1,
    kSequenceFieldNumber = 4,
  };
  // .google.protobuf.Timestamp timestamp = 2;
  bool has_timestamp() const;
  private:
  bool _internal_has_timestamp() const;
  public:
  void clear_timestamp();
  const ::PROTOBUF_NAMESPACE_ID::Timestamp& timestamp() const;
  PROTOBUF_NODISCARD ::PROTOBUF_NAMESPACE_ID::Timestamp* release_timestamp();
  ::PROTOBUF_NAMESPACE_ID::Timestamp* mutable_timestamp();
  void set_allocated_timestamp(::PROTOBUF_NAMESPACE_ID::Timestamp* timestamp);
  private:
  const ::PROTOBUF_NAMESPACE_ID::Timestamp& _internal_timestamp() const;
  ::PROTOBUF_NAMESPACE_ID::Timestamp* _internal_mutable_timestamp();
  public:
  void unsafe_arena_set_allocated_timestamp(
      ::PROTOBUF_NAMESPACE_ID::Timestamp* timestamp);
  ::PROTOBUF_NAMESPACE_ID::Timestamp* unsafe_arena_release_timestamp();

  // .gameserver.Token token = 3;
  bool has_token() const;
  private:
  bool _internal_has_token() const;
  public:
  void clear_token();
  const ::gameserver::Token& token() const;
  PROTOBUF_NODISCARD ::gameserver::Token* release_token();
  ::gameserver::Token* mutable_token();
  void set_allocated_token(::gameserver::Token* token);
  private:
  const ::gameserver::Token& _internal_token() const;
  ::gameserver::Token* _internal_mutable_token();
  public:
  void unsafe_arena_set_allocated_token(
      ::gameserver::Token* token);
  ::gameserver::Token* unsafe_arena_release_token();

  // .gameserver.PACKET_TYPE packet_type = 1;
  void clear_packet_type();
  ::gameserver::PACKET_TYPE packet_type() const;
  void set_packet_type(::gameserver::PACKET_TYPE value);
  private:
  ::gameserver::PACKET_TYPE _internal_packet_type() const;
  void _internal_set_packet_type(::gameserver::PACKET_TYPE value);
  public:

  // fixed32 sequence = 4;
  void clear_sequence();
  uint32_t sequence() const;
  void set_sequence(uint32_t value);
  private:
  uint32_t _internal_sequence() const;
  void _internal_set_sequence(uint32_t value);
  public:

  // @@protoc_insertion_point(class_scope:gameserver.Packet)
 private:
  class _Internal;

  template <typename T> friend class ::PROTOBUF_NAMESPACE_ID::Arena::InternalHelper;
  typedef void InternalArenaConstructable_;
  typedef void DestructorSkippable_;
  struct Impl_ {
    ::PROTOBUF_NAMESPACE_ID::Timestamp* timestamp_;
    ::gameserver::Token* token_;
    int packet_type_;
    uint32_t sequence_;
    mutable ::PROTOBUF_NAMESPACE_ID::internal::CachedSize _cached_size_;
  };
  union { Impl_ _impl_; };
  friend struct ::TableStruct_packet_2eproto;
};
// ===================================================================


// ===================================================================

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif  // __GNUC__
// Packet

// .gameserver.PACKET_TYPE packet_type = 1;
inline void Packet::clear_packet_type() {
  _impl_.packet_type_ = 0;
}
inline ::gameserver::PACKET_TYPE Packet::_internal_packet_type() const {
  return static_cast< ::gameserver::PACKET_TYPE >(_impl_.packet_type_);
}
inline ::gameserver::PACKET_TYPE Packet::packet_type() const {
  // @@protoc_insertion_point(field_get:gameserver.Packet.packet_type)
  return _internal_packet_type();
}
inline void Packet::_internal_set_packet_type(::gameserver::PACKET_TYPE value) {
  
  _impl_.packet_type_ = value;
}
inline void Packet::set_packet_type(::gameserver::PACKET_TYPE value) {
  _internal_set_packet_type(value);
  // @@protoc_insertion_point(field_set:gameserver.Packet.packet_type)
}

// .google.protobuf.Timestamp timestamp = 2;
inline bool Packet::_internal_has_timestamp() const {
  return this != internal_default_instance() && _impl_.timestamp_ != nullptr;
}
inline bool Packet::has_timestamp() const {
  return _internal_has_timestamp();
}
inline const ::PROTOBUF_NAMESPACE_ID::Timestamp& Packet::_internal_timestamp() const {
  const ::PROTOBUF_NAMESPACE_ID::Timestamp* p = _impl_.timestamp_;
  return p != nullptr ? *p : reinterpret_cast<const ::PROTOBUF_NAMESPACE_ID::Timestamp&>(
      ::PROTOBUF_NAMESPACE_ID::_Timestamp_default_instance_);
}
inline const ::PROTOBUF_NAMESPACE_ID::Timestamp& Packet::timestamp() const {
  // @@protoc_insertion_point(field_get:gameserver.Packet.timestamp)
  return _internal_timestamp();
}
inline void Packet::unsafe_arena_set_allocated_timestamp(
    ::PROTOBUF_NAMESPACE_ID::Timestamp* timestamp) {
  if (GetArenaForAllocation() == nullptr) {
    delete reinterpret_cast<::PROTOBUF_NAMESPACE_ID::MessageLite*>(_impl_.timestamp_);
  }
  _impl_.timestamp_ = timestamp;
  if (timestamp) {
    
  } else {
    
  }
  // @@protoc_insertion_point(field_unsafe_arena_set_allocated:gameserver.Packet.timestamp)
}
inline ::PROTOBUF_NAMESPACE_ID::Timestamp* Packet::release_timestamp() {
  
  ::PROTOBUF_NAMESPACE_ID::Timestamp* temp = _impl_.timestamp_;
  _impl_.timestamp_ = nullptr;
#ifdef PROTOBUF_FORCE_COPY_IN_RELEASE
  auto* old =  reinterpret_cast<::PROTOBUF_NAMESPACE_ID::MessageLite*>(temp);
  temp = ::PROTOBUF_NAMESPACE_ID::internal::DuplicateIfNonNull(temp);
  if (GetArenaForAllocation() == nullptr) { delete old; }
#else  // PROTOBUF_FORCE_COPY_IN_RELEASE
  if (GetArenaForAllocation() != nullptr) {
    temp = ::PROTOBUF_NAMESPACE_ID::internal::DuplicateIfNonNull(temp);
  }
#endif  // !PROTOBUF_FORCE_COPY_IN_RELEASE
  return temp;
}
inline ::PROTOBUF_NAMESPACE_ID::Timestamp* Packet::unsafe_arena_release_timestamp() {
  // @@protoc_insertion_point(field_release:gameserver.Packet.timestamp)
  
  ::PROTOBUF_NAMESPACE_ID::Timestamp* temp = _impl_.timestamp_;
  _impl_.timestamp_ = nullptr;
  return temp;
}
inline ::PROTOBUF_NAMESPACE_ID::Timestamp* Packet::_internal_mutable_timestamp() {
  
  if (_impl_.timestamp_ == nullptr) {
    auto* p = CreateMaybeMessage<::PROTOBUF_NAMESPACE_ID::Timestamp>(GetArenaForAllocation());
    _impl_.timestamp_ = p;
  }
  return _impl_.timestamp_;
}
inline ::PROTOBUF_NAMESPACE_ID::Timestamp* Packet::mutable_timestamp() {
  ::PROTOBUF_NAMESPACE_ID::Timestamp* _msg = _internal_mutable_timestamp();
  // @@protoc_insertion_point(field_mutable:gameserver.Packet.timestamp)
  return _msg;
}
inline void Packet::set_allocated_timestamp(::PROTOBUF_NAMESPACE_ID::Timestamp* timestamp) {
  ::PROTOBUF_NAMESPACE_ID::Arena* message_arena = GetArenaForAllocation();
  if (message_arena == nullptr) {
    delete reinterpret_cast< ::PROTOBUF_NAMESPACE_ID::MessageLite*>(_impl_.timestamp_);
  }
  if (timestamp) {
    ::PROTOBUF_NAMESPACE_ID::Arena* submessage_arena =
        ::PROTOBUF_NAMESPACE_ID::Arena::InternalGetOwningArena(
                reinterpret_cast<::PROTOBUF_NAMESPACE_ID::MessageLite*>(timestamp));
    if (message_arena != submessage_arena) {
      timestamp = ::PROTOBUF_NAMESPACE_ID::internal::GetOwnedMessage(
          message_arena, timestamp, submessage_arena);
    }
    
  } else {
    
  }
  _impl_.timestamp_ = timestamp;
  // @@protoc_insertion_point(field_set_allocated:gameserver.Packet.timestamp)
}

// .gameserver.Token token = 3;
inline bool Packet::_internal_has_token() const {
  return this != internal_default_instance() && _impl_.token_ != nullptr;
}
inline bool Packet::has_token() const {
  return _internal_has_token();
}
inline const ::gameserver::Token& Packet::_internal_token() const {
  const ::gameserver::Token* p = _impl_.token_;
  return p != nullptr ? *p : reinterpret_cast<const ::gameserver::Token&>(
      ::gameserver::_Token_default_instance_);
}
inline const ::gameserver::Token& Packet::token() const {
  // @@protoc_insertion_point(field_get:gameserver.Packet.token)
  return _internal_token();
}
inline void Packet::unsafe_arena_set_allocated_token(
    ::gameserver::Token* token) {
  if (GetArenaForAllocation() == nullptr) {
    delete reinterpret_cast<::PROTOBUF_NAMESPACE_ID::MessageLite*>(_impl_.token_);
  }
  _impl_.token_ = token;
  if (token) {
    
  } else {
    
  }
  // @@protoc_insertion_point(field_unsafe_arena_set_allocated:gameserver.Packet.token)
}
inline ::gameserver::Token* Packet::release_token() {
  
  ::gameserver::Token* temp = _impl_.token_;
  _impl_.token_ = nullptr;
#ifdef PROTOBUF_FORCE_COPY_IN_RELEASE
  auto* old =  reinterpret_cast<::PROTOBUF_NAMESPACE_ID::MessageLite*>(temp);
  temp = ::PROTOBUF_NAMESPACE_ID::internal::DuplicateIfNonNull(temp);
  if (GetArenaForAllocation() == nullptr) { delete old; }
#else  // PROTOBUF_FORCE_COPY_IN_RELEASE
  if (GetArenaForAllocation() != nullptr) {
    temp = ::PROTOBUF_NAMESPACE_ID::internal::DuplicateIfNonNull(temp);
  }
#endif  // !PROTOBUF_FORCE_COPY_IN_RELEASE
  return temp;
}
inline ::gameserver::Token* Packet::unsafe_arena_release_token() {
  // @@protoc_insertion_point(field_release:gameserver.Packet.token)
  
  ::gameserver::Token* temp = _impl_.token_;
  _impl_.token_ = nullptr;
  return temp;
}
inline ::gameserver::Token* Packet::_internal_mutable_token() {
  
  if (_impl_.token_ == nullptr) {
    auto* p = CreateMaybeMessage<::gameserver::Token>(GetArenaForAllocation());
    _impl_.token_ = p;
  }
  return _impl_.token_;
}
inline ::gameserver::Token* Packet::mutable_token() {
  ::gameserver::Token* _msg = _internal_mutable_token();
  // @@protoc_insertion_point(field_mutable:gameserver.Packet.token)
  return _msg;
}
inline void Packet::set_allocated_token(::gameserver::Token* token) {
  ::PROTOBUF_NAMESPACE_ID::Arena* message_arena = GetArenaForAllocation();
  if (message_arena == nullptr) {
    delete reinterpret_cast< ::PROTOBUF_NAMESPACE_ID::MessageLite*>(_impl_.token_);
  }
  if (token) {
    ::PROTOBUF_NAMESPACE_ID::Arena* submessage_arena =
        ::PROTOBUF_NAMESPACE_ID::Arena::InternalGetOwningArena(
                reinterpret_cast<::PROTOBUF_NAMESPACE_ID::MessageLite*>(token));
    if (message_arena != submessage_arena) {
      token = ::PROTOBUF_NAMESPACE_ID::internal::GetOwnedMessage(
          message_arena, token, submessage_arena);
    }
    
  } else {
    
  }
  _impl_.token_ = token;
  // @@protoc_insertion_point(field_set_allocated:gameserver.Packet.token)
}

// fixed32 sequence = 4;
inline void Packet::clear_sequence() {
  _impl_.sequence_ = 0u;
}
inline uint32_t Packet::_internal_sequence() const {
  return _impl_.sequence_;
}
inline uint32_t Packet::sequence() const {
  // @@protoc_insertion_point(field_get:gameserver.Packet.sequence)
  return _internal_sequence();
}
inline void Packet::_internal_set_sequence(uint32_t value) {
  
  _impl_.sequence_ = value;
}
inline void Packet::set_sequence(uint32_t value) {
  _internal_set_sequence(value);
  // @@protoc_insertion_point(field_set:gameserver.Packet.sequence)
}

#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif  // __GNUC__

// @@protoc_insertion_point(namespace_scope)

}  // namespace gameserver

// @@protoc_insertion_point(global_scope)

#include <google/protobuf/port_undef.inc>
#endif  // GOOGLE_PROTOBUF_INCLUDED_GOOGLE_PROTOBUF_INCLUDED_packet_2eproto
