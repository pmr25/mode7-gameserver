// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: dh_key_exch.proto

#ifndef GOOGLE_PROTOBUF_INCLUDED_dh_5fkey_5fexch_2eproto
#define GOOGLE_PROTOBUF_INCLUDED_dh_5fkey_5fexch_2eproto

#include <limits>
#include <string>

#include <google/protobuf/port_def.inc>
#if PROTOBUF_VERSION < 3021000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers. Please update
#error your headers.
#endif
#if 3021012 < PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers. Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/port_undef.inc>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/metadata_lite.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>  // IWYU pragma: export
#include <google/protobuf/extension_set.h>  // IWYU pragma: export
#include <google/protobuf/unknown_field_set.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
#define PROTOBUF_INTERNAL_EXPORT_dh_5fkey_5fexch_2eproto
PROTOBUF_NAMESPACE_OPEN
namespace internal {
class AnyMetadata;
}  // namespace internal
PROTOBUF_NAMESPACE_CLOSE

// Internal implementation detail -- do not use these members.
struct TableStruct_dh_5fkey_5fexch_2eproto {
  static const uint32_t offsets[];
};
extern const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_dh_5fkey_5fexch_2eproto;
namespace gameserver {
class DHKeyExch;
struct DHKeyExchDefaultTypeInternal;
extern DHKeyExchDefaultTypeInternal _DHKeyExch_default_instance_;
}  // namespace gameserver
PROTOBUF_NAMESPACE_OPEN
template<> ::gameserver::DHKeyExch* Arena::CreateMaybeMessage<::gameserver::DHKeyExch>(Arena*);
PROTOBUF_NAMESPACE_CLOSE
namespace gameserver {

// ===================================================================

class DHKeyExch final :
    public ::PROTOBUF_NAMESPACE_ID::Message /* @@protoc_insertion_point(class_definition:gameserver.DHKeyExch) */ {
 public:
  inline DHKeyExch() : DHKeyExch(nullptr) {}
  ~DHKeyExch() override;
  explicit PROTOBUF_CONSTEXPR DHKeyExch(::PROTOBUF_NAMESPACE_ID::internal::ConstantInitialized);

  DHKeyExch(const DHKeyExch& from);
  DHKeyExch(DHKeyExch&& from) noexcept
    : DHKeyExch() {
    *this = ::std::move(from);
  }

  inline DHKeyExch& operator=(const DHKeyExch& from) {
    CopyFrom(from);
    return *this;
  }
  inline DHKeyExch& operator=(DHKeyExch&& from) noexcept {
    if (this == &from) return *this;
    if (GetOwningArena() == from.GetOwningArena()
  #ifdef PROTOBUF_FORCE_COPY_IN_MOVE
        && GetOwningArena() != nullptr
  #endif  // !PROTOBUF_FORCE_COPY_IN_MOVE
    ) {
      InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }

  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* descriptor() {
    return GetDescriptor();
  }
  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* GetDescriptor() {
    return default_instance().GetMetadata().descriptor;
  }
  static const ::PROTOBUF_NAMESPACE_ID::Reflection* GetReflection() {
    return default_instance().GetMetadata().reflection;
  }
  static const DHKeyExch& default_instance() {
    return *internal_default_instance();
  }
  static inline const DHKeyExch* internal_default_instance() {
    return reinterpret_cast<const DHKeyExch*>(
               &_DHKeyExch_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    0;

  friend void swap(DHKeyExch& a, DHKeyExch& b) {
    a.Swap(&b);
  }
  inline void Swap(DHKeyExch* other) {
    if (other == this) return;
  #ifdef PROTOBUF_FORCE_COPY_IN_SWAP
    if (GetOwningArena() != nullptr &&
        GetOwningArena() == other->GetOwningArena()) {
   #else  // PROTOBUF_FORCE_COPY_IN_SWAP
    if (GetOwningArena() == other->GetOwningArena()) {
  #endif  // !PROTOBUF_FORCE_COPY_IN_SWAP
      InternalSwap(other);
    } else {
      ::PROTOBUF_NAMESPACE_ID::internal::GenericSwap(this, other);
    }
  }
  void UnsafeArenaSwap(DHKeyExch* other) {
    if (other == this) return;
    GOOGLE_DCHECK(GetOwningArena() == other->GetOwningArena());
    InternalSwap(other);
  }

  // implements Message ----------------------------------------------

  DHKeyExch* New(::PROTOBUF_NAMESPACE_ID::Arena* arena = nullptr) const final {
    return CreateMaybeMessage<DHKeyExch>(arena);
  }
  using ::PROTOBUF_NAMESPACE_ID::Message::CopyFrom;
  void CopyFrom(const DHKeyExch& from);
  using ::PROTOBUF_NAMESPACE_ID::Message::MergeFrom;
  void MergeFrom( const DHKeyExch& from) {
    DHKeyExch::MergeImpl(*this, from);
  }
  private:
  static void MergeImpl(::PROTOBUF_NAMESPACE_ID::Message& to_msg, const ::PROTOBUF_NAMESPACE_ID::Message& from_msg);
  public:
  PROTOBUF_ATTRIBUTE_REINITIALIZES void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  const char* _InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) final;
  uint8_t* _InternalSerialize(
      uint8_t* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const final;
  int GetCachedSize() const final { return _impl_._cached_size_.Get(); }

  private:
  void SharedCtor(::PROTOBUF_NAMESPACE_ID::Arena* arena, bool is_message_owned);
  void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(DHKeyExch* other);

  private:
  friend class ::PROTOBUF_NAMESPACE_ID::internal::AnyMetadata;
  static ::PROTOBUF_NAMESPACE_ID::StringPiece FullMessageName() {
    return "gameserver.DHKeyExch";
  }
  protected:
  explicit DHKeyExch(::PROTOBUF_NAMESPACE_ID::Arena* arena,
                       bool is_message_owned = false);
  public:

  static const ClassData _class_data_;
  const ::PROTOBUF_NAMESPACE_ID::Message::ClassData*GetClassData() const final;

  ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadata() const final;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  enum : int {
    kClientMaterialFieldNumber = 1,
    kServerMaterialFieldNumber = 2,
  };
  // string client_material = 1;
  void clear_client_material();
  const std::string& client_material() const;
  template <typename ArgT0 = const std::string&, typename... ArgT>
  void set_client_material(ArgT0&& arg0, ArgT... args);
  std::string* mutable_client_material();
  PROTOBUF_NODISCARD std::string* release_client_material();
  void set_allocated_client_material(std::string* client_material);
  private:
  const std::string& _internal_client_material() const;
  inline PROTOBUF_ALWAYS_INLINE void _internal_set_client_material(const std::string& value);
  std::string* _internal_mutable_client_material();
  public:

  // string server_material = 2;
  void clear_server_material();
  const std::string& server_material() const;
  template <typename ArgT0 = const std::string&, typename... ArgT>
  void set_server_material(ArgT0&& arg0, ArgT... args);
  std::string* mutable_server_material();
  PROTOBUF_NODISCARD std::string* release_server_material();
  void set_allocated_server_material(std::string* server_material);
  private:
  const std::string& _internal_server_material() const;
  inline PROTOBUF_ALWAYS_INLINE void _internal_set_server_material(const std::string& value);
  std::string* _internal_mutable_server_material();
  public:

  // @@protoc_insertion_point(class_scope:gameserver.DHKeyExch)
 private:
  class _Internal;

  template <typename T> friend class ::PROTOBUF_NAMESPACE_ID::Arena::InternalHelper;
  typedef void InternalArenaConstructable_;
  typedef void DestructorSkippable_;
  struct Impl_ {
    ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr client_material_;
    ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr server_material_;
    mutable ::PROTOBUF_NAMESPACE_ID::internal::CachedSize _cached_size_;
  };
  union { Impl_ _impl_; };
  friend struct ::TableStruct_dh_5fkey_5fexch_2eproto;
};
// ===================================================================


// ===================================================================

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif  // __GNUC__
// DHKeyExch

// string client_material = 1;
inline void DHKeyExch::clear_client_material() {
  _impl_.client_material_.ClearToEmpty();
}
inline const std::string& DHKeyExch::client_material() const {
  // @@protoc_insertion_point(field_get:gameserver.DHKeyExch.client_material)
  return _internal_client_material();
}
template <typename ArgT0, typename... ArgT>
inline PROTOBUF_ALWAYS_INLINE
void DHKeyExch::set_client_material(ArgT0&& arg0, ArgT... args) {
 
 _impl_.client_material_.Set(static_cast<ArgT0 &&>(arg0), args..., GetArenaForAllocation());
  // @@protoc_insertion_point(field_set:gameserver.DHKeyExch.client_material)
}
inline std::string* DHKeyExch::mutable_client_material() {
  std::string* _s = _internal_mutable_client_material();
  // @@protoc_insertion_point(field_mutable:gameserver.DHKeyExch.client_material)
  return _s;
}
inline const std::string& DHKeyExch::_internal_client_material() const {
  return _impl_.client_material_.Get();
}
inline void DHKeyExch::_internal_set_client_material(const std::string& value) {
  
  _impl_.client_material_.Set(value, GetArenaForAllocation());
}
inline std::string* DHKeyExch::_internal_mutable_client_material() {
  
  return _impl_.client_material_.Mutable(GetArenaForAllocation());
}
inline std::string* DHKeyExch::release_client_material() {
  // @@protoc_insertion_point(field_release:gameserver.DHKeyExch.client_material)
  return _impl_.client_material_.Release();
}
inline void DHKeyExch::set_allocated_client_material(std::string* client_material) {
  if (client_material != nullptr) {
    
  } else {
    
  }
  _impl_.client_material_.SetAllocated(client_material, GetArenaForAllocation());
#ifdef PROTOBUF_FORCE_COPY_DEFAULT_STRING
  if (_impl_.client_material_.IsDefault()) {
    _impl_.client_material_.Set("", GetArenaForAllocation());
  }
#endif // PROTOBUF_FORCE_COPY_DEFAULT_STRING
  // @@protoc_insertion_point(field_set_allocated:gameserver.DHKeyExch.client_material)
}

// string server_material = 2;
inline void DHKeyExch::clear_server_material() {
  _impl_.server_material_.ClearToEmpty();
}
inline const std::string& DHKeyExch::server_material() const {
  // @@protoc_insertion_point(field_get:gameserver.DHKeyExch.server_material)
  return _internal_server_material();
}
template <typename ArgT0, typename... ArgT>
inline PROTOBUF_ALWAYS_INLINE
void DHKeyExch::set_server_material(ArgT0&& arg0, ArgT... args) {
 
 _impl_.server_material_.Set(static_cast<ArgT0 &&>(arg0), args..., GetArenaForAllocation());
  // @@protoc_insertion_point(field_set:gameserver.DHKeyExch.server_material)
}
inline std::string* DHKeyExch::mutable_server_material() {
  std::string* _s = _internal_mutable_server_material();
  // @@protoc_insertion_point(field_mutable:gameserver.DHKeyExch.server_material)
  return _s;
}
inline const std::string& DHKeyExch::_internal_server_material() const {
  return _impl_.server_material_.Get();
}
inline void DHKeyExch::_internal_set_server_material(const std::string& value) {
  
  _impl_.server_material_.Set(value, GetArenaForAllocation());
}
inline std::string* DHKeyExch::_internal_mutable_server_material() {
  
  return _impl_.server_material_.Mutable(GetArenaForAllocation());
}
inline std::string* DHKeyExch::release_server_material() {
  // @@protoc_insertion_point(field_release:gameserver.DHKeyExch.server_material)
  return _impl_.server_material_.Release();
}
inline void DHKeyExch::set_allocated_server_material(std::string* server_material) {
  if (server_material != nullptr) {
    
  } else {
    
  }
  _impl_.server_material_.SetAllocated(server_material, GetArenaForAllocation());
#ifdef PROTOBUF_FORCE_COPY_DEFAULT_STRING
  if (_impl_.server_material_.IsDefault()) {
    _impl_.server_material_.Set("", GetArenaForAllocation());
  }
#endif // PROTOBUF_FORCE_COPY_DEFAULT_STRING
  // @@protoc_insertion_point(field_set_allocated:gameserver.DHKeyExch.server_material)
}

#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif  // __GNUC__

// @@protoc_insertion_point(namespace_scope)

}  // namespace gameserver

// @@protoc_insertion_point(global_scope)

#include <google/protobuf/port_undef.inc>
#endif  // GOOGLE_PROTOBUF_INCLUDED_GOOGLE_PROTOBUF_INCLUDED_dh_5fkey_5fexch_2eproto
