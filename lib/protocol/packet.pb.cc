// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: packet.proto

#include "packet.pb.h"

#include <algorithm>

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/wire_format_lite.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>

PROTOBUF_PRAGMA_INIT_SEG

namespace _pb = ::PROTOBUF_NAMESPACE_ID;
namespace _pbi = _pb::internal;

namespace gameserver {
PROTOBUF_CONSTEXPR Packet::Packet(
    ::_pbi::ConstantInitialized): _impl_{
    /*decltype(_impl_.timestamp_)*/nullptr
  , /*decltype(_impl_.token_)*/nullptr
  , /*decltype(_impl_.packet_type_)*/0
  , /*decltype(_impl_.sequence_)*/0u
  , /*decltype(_impl_._cached_size_)*/{}} {}
struct PacketDefaultTypeInternal {
  PROTOBUF_CONSTEXPR PacketDefaultTypeInternal()
      : _instance(::_pbi::ConstantInitialized{}) {}
  ~PacketDefaultTypeInternal() {}
  union {
    Packet _instance;
  };
};
PROTOBUF_ATTRIBUTE_NO_DESTROY PROTOBUF_CONSTINIT PROTOBUF_ATTRIBUTE_INIT_PRIORITY1 PacketDefaultTypeInternal _Packet_default_instance_;
}  // namespace gameserver
static ::_pb::Metadata file_level_metadata_packet_2eproto[1];
static constexpr ::_pb::EnumDescriptor const** file_level_enum_descriptors_packet_2eproto = nullptr;
static constexpr ::_pb::ServiceDescriptor const** file_level_service_descriptors_packet_2eproto = nullptr;

const uint32_t TableStruct_packet_2eproto::offsets[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  ~0u,  // no _has_bits_
  PROTOBUF_FIELD_OFFSET(::gameserver::Packet, _internal_metadata_),
  ~0u,  // no _extensions_
  ~0u,  // no _oneof_case_
  ~0u,  // no _weak_field_map_
  ~0u,  // no _inlined_string_donated_
  PROTOBUF_FIELD_OFFSET(::gameserver::Packet, _impl_.packet_type_),
  PROTOBUF_FIELD_OFFSET(::gameserver::Packet, _impl_.timestamp_),
  PROTOBUF_FIELD_OFFSET(::gameserver::Packet, _impl_.token_),
  PROTOBUF_FIELD_OFFSET(::gameserver::Packet, _impl_.sequence_),
};
static const ::_pbi::MigrationSchema schemas[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  { 0, -1, -1, sizeof(::gameserver::Packet)},
};

static const ::_pb::Message* const file_default_instances[] = {
  &::gameserver::_Packet_default_instance_._instance,
};

const char descriptor_table_protodef_packet_2eproto[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) =
  "\n\014packet.proto\022\ngameserver\032\037google/proto"
  "buf/timestamp.proto\032\021packet_type.proto\032\013"
  "token.proto\"\231\001\n\006Packet\022,\n\013packet_type\030\001 "
  "\001(\0162\027.gameserver.PACKET_TYPE\022-\n\ttimestam"
  "p\030\002 \001(\0132\032.google.protobuf.Timestamp\022 \n\005t"
  "oken\030\003 \001(\0132\021.gameserver.Token\022\020\n\010sequenc"
  "e\030\004 \001(\007B\016Z\014pkg/protocolb\006proto3"
  ;
static const ::_pbi::DescriptorTable* const descriptor_table_packet_2eproto_deps[3] = {
  &::descriptor_table_google_2fprotobuf_2ftimestamp_2eproto,
  &::descriptor_table_packet_5ftype_2eproto,
  &::descriptor_table_token_2eproto,
};
static ::_pbi::once_flag descriptor_table_packet_2eproto_once;
const ::_pbi::DescriptorTable descriptor_table_packet_2eproto = {
    false, false, 271, descriptor_table_protodef_packet_2eproto,
    "packet.proto",
    &descriptor_table_packet_2eproto_once, descriptor_table_packet_2eproto_deps, 3, 1,
    schemas, file_default_instances, TableStruct_packet_2eproto::offsets,
    file_level_metadata_packet_2eproto, file_level_enum_descriptors_packet_2eproto,
    file_level_service_descriptors_packet_2eproto,
};
PROTOBUF_ATTRIBUTE_WEAK const ::_pbi::DescriptorTable* descriptor_table_packet_2eproto_getter() {
  return &descriptor_table_packet_2eproto;
}

// Force running AddDescriptors() at dynamic initialization time.
PROTOBUF_ATTRIBUTE_INIT_PRIORITY2 static ::_pbi::AddDescriptorsRunner dynamic_init_dummy_packet_2eproto(&descriptor_table_packet_2eproto);
namespace gameserver {

// ===================================================================

class Packet::_Internal {
 public:
  static const ::PROTOBUF_NAMESPACE_ID::Timestamp& timestamp(const Packet* msg);
  static const ::gameserver::Token& token(const Packet* msg);
};

const ::PROTOBUF_NAMESPACE_ID::Timestamp&
Packet::_Internal::timestamp(const Packet* msg) {
  return *msg->_impl_.timestamp_;
}
const ::gameserver::Token&
Packet::_Internal::token(const Packet* msg) {
  return *msg->_impl_.token_;
}
void Packet::clear_timestamp() {
  if (GetArenaForAllocation() == nullptr && _impl_.timestamp_ != nullptr) {
    delete _impl_.timestamp_;
  }
  _impl_.timestamp_ = nullptr;
}
void Packet::clear_token() {
  if (GetArenaForAllocation() == nullptr && _impl_.token_ != nullptr) {
    delete _impl_.token_;
  }
  _impl_.token_ = nullptr;
}
Packet::Packet(::PROTOBUF_NAMESPACE_ID::Arena* arena,
                         bool is_message_owned)
  : ::PROTOBUF_NAMESPACE_ID::Message(arena, is_message_owned) {
  SharedCtor(arena, is_message_owned);
  // @@protoc_insertion_point(arena_constructor:gameserver.Packet)
}
Packet::Packet(const Packet& from)
  : ::PROTOBUF_NAMESPACE_ID::Message() {
  Packet* const _this = this; (void)_this;
  new (&_impl_) Impl_{
      decltype(_impl_.timestamp_){nullptr}
    , decltype(_impl_.token_){nullptr}
    , decltype(_impl_.packet_type_){}
    , decltype(_impl_.sequence_){}
    , /*decltype(_impl_._cached_size_)*/{}};

  _internal_metadata_.MergeFrom<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(from._internal_metadata_);
  if (from._internal_has_timestamp()) {
    _this->_impl_.timestamp_ = new ::PROTOBUF_NAMESPACE_ID::Timestamp(*from._impl_.timestamp_);
  }
  if (from._internal_has_token()) {
    _this->_impl_.token_ = new ::gameserver::Token(*from._impl_.token_);
  }
  ::memcpy(&_impl_.packet_type_, &from._impl_.packet_type_,
    static_cast<size_t>(reinterpret_cast<char*>(&_impl_.sequence_) -
    reinterpret_cast<char*>(&_impl_.packet_type_)) + sizeof(_impl_.sequence_));
  // @@protoc_insertion_point(copy_constructor:gameserver.Packet)
}

inline void Packet::SharedCtor(
    ::_pb::Arena* arena, bool is_message_owned) {
  (void)arena;
  (void)is_message_owned;
  new (&_impl_) Impl_{
      decltype(_impl_.timestamp_){nullptr}
    , decltype(_impl_.token_){nullptr}
    , decltype(_impl_.packet_type_){0}
    , decltype(_impl_.sequence_){0u}
    , /*decltype(_impl_._cached_size_)*/{}
  };
}

Packet::~Packet() {
  // @@protoc_insertion_point(destructor:gameserver.Packet)
  if (auto *arena = _internal_metadata_.DeleteReturnArena<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>()) {
  (void)arena;
    return;
  }
  SharedDtor();
}

inline void Packet::SharedDtor() {
  GOOGLE_DCHECK(GetArenaForAllocation() == nullptr);
  if (this != internal_default_instance()) delete _impl_.timestamp_;
  if (this != internal_default_instance()) delete _impl_.token_;
}

void Packet::SetCachedSize(int size) const {
  _impl_._cached_size_.Set(size);
}

void Packet::Clear() {
// @@protoc_insertion_point(message_clear_start:gameserver.Packet)
  uint32_t cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  if (GetArenaForAllocation() == nullptr && _impl_.timestamp_ != nullptr) {
    delete _impl_.timestamp_;
  }
  _impl_.timestamp_ = nullptr;
  if (GetArenaForAllocation() == nullptr && _impl_.token_ != nullptr) {
    delete _impl_.token_;
  }
  _impl_.token_ = nullptr;
  ::memset(&_impl_.packet_type_, 0, static_cast<size_t>(
      reinterpret_cast<char*>(&_impl_.sequence_) -
      reinterpret_cast<char*>(&_impl_.packet_type_)) + sizeof(_impl_.sequence_));
  _internal_metadata_.Clear<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>();
}

const char* Packet::_InternalParse(const char* ptr, ::_pbi::ParseContext* ctx) {
#define CHK_(x) if (PROTOBUF_PREDICT_FALSE(!(x))) goto failure
  while (!ctx->Done(&ptr)) {
    uint32_t tag;
    ptr = ::_pbi::ReadTag(ptr, &tag);
    switch (tag >> 3) {
      // .gameserver.PACKET_TYPE packet_type = 1;
      case 1:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 8)) {
          uint64_t val = ::PROTOBUF_NAMESPACE_ID::internal::ReadVarint64(&ptr);
          CHK_(ptr);
          _internal_set_packet_type(static_cast<::gameserver::PACKET_TYPE>(val));
        } else
          goto handle_unusual;
        continue;
      // .google.protobuf.Timestamp timestamp = 2;
      case 2:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 18)) {
          ptr = ctx->ParseMessage(_internal_mutable_timestamp(), ptr);
          CHK_(ptr);
        } else
          goto handle_unusual;
        continue;
      // .gameserver.Token token = 3;
      case 3:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 26)) {
          ptr = ctx->ParseMessage(_internal_mutable_token(), ptr);
          CHK_(ptr);
        } else
          goto handle_unusual;
        continue;
      // fixed32 sequence = 4;
      case 4:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 37)) {
          _impl_.sequence_ = ::PROTOBUF_NAMESPACE_ID::internal::UnalignedLoad<uint32_t>(ptr);
          ptr += sizeof(uint32_t);
        } else
          goto handle_unusual;
        continue;
      default:
        goto handle_unusual;
    }  // switch
  handle_unusual:
    if ((tag == 0) || ((tag & 7) == 4)) {
      CHK_(ptr);
      ctx->SetLastTag(tag);
      goto message_done;
    }
    ptr = UnknownFieldParse(
        tag,
        _internal_metadata_.mutable_unknown_fields<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(),
        ptr, ctx);
    CHK_(ptr != nullptr);
  }  // while
message_done:
  return ptr;
failure:
  ptr = nullptr;
  goto message_done;
#undef CHK_
}

uint8_t* Packet::_InternalSerialize(
    uint8_t* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const {
  // @@protoc_insertion_point(serialize_to_array_start:gameserver.Packet)
  uint32_t cached_has_bits = 0;
  (void) cached_has_bits;

  // .gameserver.PACKET_TYPE packet_type = 1;
  if (this->_internal_packet_type() != 0) {
    target = stream->EnsureSpace(target);
    target = ::_pbi::WireFormatLite::WriteEnumToArray(
      1, this->_internal_packet_type(), target);
  }

  // .google.protobuf.Timestamp timestamp = 2;
  if (this->_internal_has_timestamp()) {
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::
      InternalWriteMessage(2, _Internal::timestamp(this),
        _Internal::timestamp(this).GetCachedSize(), target, stream);
  }

  // .gameserver.Token token = 3;
  if (this->_internal_has_token()) {
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::
      InternalWriteMessage(3, _Internal::token(this),
        _Internal::token(this).GetCachedSize(), target, stream);
  }

  // fixed32 sequence = 4;
  if (this->_internal_sequence() != 0) {
    target = stream->EnsureSpace(target);
    target = ::_pbi::WireFormatLite::WriteFixed32ToArray(4, this->_internal_sequence(), target);
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    target = ::_pbi::WireFormat::InternalSerializeUnknownFieldsToArray(
        _internal_metadata_.unknown_fields<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(::PROTOBUF_NAMESPACE_ID::UnknownFieldSet::default_instance), target, stream);
  }
  // @@protoc_insertion_point(serialize_to_array_end:gameserver.Packet)
  return target;
}

size_t Packet::ByteSizeLong() const {
// @@protoc_insertion_point(message_byte_size_start:gameserver.Packet)
  size_t total_size = 0;

  uint32_t cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  // .google.protobuf.Timestamp timestamp = 2;
  if (this->_internal_has_timestamp()) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::MessageSize(
        *_impl_.timestamp_);
  }

  // .gameserver.Token token = 3;
  if (this->_internal_has_token()) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::MessageSize(
        *_impl_.token_);
  }

  // .gameserver.PACKET_TYPE packet_type = 1;
  if (this->_internal_packet_type() != 0) {
    total_size += 1 +
      ::_pbi::WireFormatLite::EnumSize(this->_internal_packet_type());
  }

  // fixed32 sequence = 4;
  if (this->_internal_sequence() != 0) {
    total_size += 1 + 4;
  }

  return MaybeComputeUnknownFieldsSize(total_size, &_impl_._cached_size_);
}

const ::PROTOBUF_NAMESPACE_ID::Message::ClassData Packet::_class_data_ = {
    ::PROTOBUF_NAMESPACE_ID::Message::CopyWithSourceCheck,
    Packet::MergeImpl
};
const ::PROTOBUF_NAMESPACE_ID::Message::ClassData*Packet::GetClassData() const { return &_class_data_; }


void Packet::MergeImpl(::PROTOBUF_NAMESPACE_ID::Message& to_msg, const ::PROTOBUF_NAMESPACE_ID::Message& from_msg) {
  auto* const _this = static_cast<Packet*>(&to_msg);
  auto& from = static_cast<const Packet&>(from_msg);
  // @@protoc_insertion_point(class_specific_merge_from_start:gameserver.Packet)
  GOOGLE_DCHECK_NE(&from, _this);
  uint32_t cached_has_bits = 0;
  (void) cached_has_bits;

  if (from._internal_has_timestamp()) {
    _this->_internal_mutable_timestamp()->::PROTOBUF_NAMESPACE_ID::Timestamp::MergeFrom(
        from._internal_timestamp());
  }
  if (from._internal_has_token()) {
    _this->_internal_mutable_token()->::gameserver::Token::MergeFrom(
        from._internal_token());
  }
  if (from._internal_packet_type() != 0) {
    _this->_internal_set_packet_type(from._internal_packet_type());
  }
  if (from._internal_sequence() != 0) {
    _this->_internal_set_sequence(from._internal_sequence());
  }
  _this->_internal_metadata_.MergeFrom<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(from._internal_metadata_);
}

void Packet::CopyFrom(const Packet& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:gameserver.Packet)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool Packet::IsInitialized() const {
  return true;
}

void Packet::InternalSwap(Packet* other) {
  using std::swap;
  _internal_metadata_.InternalSwap(&other->_internal_metadata_);
  ::PROTOBUF_NAMESPACE_ID::internal::memswap<
      PROTOBUF_FIELD_OFFSET(Packet, _impl_.sequence_)
      + sizeof(Packet::_impl_.sequence_)
      - PROTOBUF_FIELD_OFFSET(Packet, _impl_.timestamp_)>(
          reinterpret_cast<char*>(&_impl_.timestamp_),
          reinterpret_cast<char*>(&other->_impl_.timestamp_));
}

::PROTOBUF_NAMESPACE_ID::Metadata Packet::GetMetadata() const {
  return ::_pbi::AssignDescriptors(
      &descriptor_table_packet_2eproto_getter, &descriptor_table_packet_2eproto_once,
      file_level_metadata_packet_2eproto[0]);
}

// @@protoc_insertion_point(namespace_scope)
}  // namespace gameserver
PROTOBUF_NAMESPACE_OPEN
template<> PROTOBUF_NOINLINE ::gameserver::Packet*
Arena::CreateMaybeMessage< ::gameserver::Packet >(Arena* arena) {
  return Arena::CreateMessageInternal< ::gameserver::Packet >(arena);
}
PROTOBUF_NAMESPACE_CLOSE

// @@protoc_insertion_point(global_scope)
#include <google/protobuf/port_undef.inc>
