#ifndef LINUXSOCKET_HPP
#define LINUXSOCKET_HPP

#ifdef __linux__

#include "Socket.hpp"

#include <iostream>
#include <cstdint>
#include <netdb.h>
#include <netinet/in.h>
#include <string>
#include <vector>

namespace udpclient
{

class LinuxSocket : public Socket
{
public:
    LinuxSocket(const std::string& serverAddr, uint16_t serverPort)
    {
        Open(serverAddr, serverPort);
    }

    virtual ~LinuxSocket() = default;

    void Open(const std::string&, uint16_t) override;
    void Close() override;
    void Read(std::vector<uint8_t>&, uint32_t) override;
    void Write(const std::vector<uint8_t>&) override;
    bool Ok() override;
    std::string GetLatestError() override
    {
        if (mErrors.size() == 0)
        {
            return "no errors";
        }

        std::string out = mErrors[mErrors.size() - 1];
        mErrors.pop_back();
        return out;
    }

private:
    int mSockfd;
    socklen_t mSockLen;
    int mPort;
    struct sockaddr_in mServAddr;
    struct hostent* mServer;
    std::vector<std::string> mErrors;
    uint32_t mPacketSize{DEFAULT_PACKET_SIZE};
};

}

#endif /* __linux__ */

#endif /* LINUXSOCKET_HPP */
