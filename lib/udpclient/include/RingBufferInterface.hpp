#ifndef RINGBUFFERINTERFACE_HPP
#define RINGBUFFERINTERFACE_HPP

namespace udpclient
{

template <typename T>
class RingBufferInterface
{
public:
    virtual auto push(T) -> void = 0;
    virtual auto pop() -> void = 0;
    virtual auto peek() -> T = 0;
};

}

#endif /* RINGBUFFERINTERFACE_HPP */
