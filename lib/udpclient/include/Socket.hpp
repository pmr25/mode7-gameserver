#ifndef SOCKET_HPP
#define SOCKET_HPP

#include <cstdint>
#include <string>
#include <vector>

#define DEFAULT_PACKET_SIZE 1024

namespace udpclient
{

class Socket
{
public:
    virtual void Open(const std::string&, uint16_t) {}
    virtual void Close() {}
    virtual void Read(std::vector<uint8_t>&, uint32_t) = 0;
    virtual void Write(const std::vector<uint8_t>&) = 0;
    virtual bool Ok() = 0;
    virtual std::string GetLatestError() = 0;
};

}

#endif /* SOCKET_HPP #ifndef SOCKET_HPP */
