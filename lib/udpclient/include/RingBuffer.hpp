#ifndef RINGBUFFER_HPP
#define RINGBUFFER_HPP

#include "RingBufferInterface.hpp"

#include <cassert>
#include <cstdint>
#include <vector>

namespace udpclient
{

template <typename T>
class RingBuffer : public RingBufferInterface<T>
{
public:
    RingBuffer(uint32_t size)
        : mSize(size)
    {
        mBuffer.reserve(mSize);
    }

    auto push(T item) -> void
    {
        if (mSize == 0)
        {
            return;
        }

        mBuffer[mHead] = item;
        mHead = increment(mHead);
    }

    auto pop() -> void
    {
        if (mTail == increment(mHead))
        {
            return;
        }

        auto top = peek();
        mTail = increment(mTail);
    }

    auto peek() -> T
    {
        return mBuffer[mTail];
    }

private:
    auto increment(uint32_t val) -> uint32_t
    {
        assert(mSize != 0);
        return (val + 1) % mSize;
    }

    uint32_t mSize;
    int32_t mHead = 0;
    int32_t mTail = 0;
    std::vector<T> mBuffer;
};

}

#endif /* RINGBUFFER_HPP */
