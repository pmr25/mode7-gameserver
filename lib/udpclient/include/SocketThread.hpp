#ifndef SOCKETTHREAD_HPP
#define SOCKETTHREAD_HPP

namespace udpclient
{

class SocketThread
{
public:
    auto operator()() -> void;

private:
    auto run() -> void;
};

}

#endif /* SOCKETTHREAD_HPP */
