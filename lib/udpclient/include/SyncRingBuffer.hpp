#ifndef SYNCRINGBUFFER_HPP
#define SYNCRINGBUFFER_HPP

#include "RingBufferInterface.hpp"
#include "RingBuffer.hpp"

#include <cstdint>
#include <memory>
#include <semaphore>

namespace udpclient
{

template <typename T>
class SyncRingBuffer : public RingBufferInterface<T>
{
public:
    SyncRingBuffer(uint32_t size)
    {
        mBuffer = std::make_unique<RingBuffer<T>>(size);
    }

    auto push(T item) -> void
    {
        assert(mBuffer != nullptr);
        
        mBufferSemaphore.acquire();
        mBuffer->push(item);
        mBufferSemaphore.release();
    }

    auto pop() -> void
    {
        assert(mBuffer != nullptr);
        
        mBufferSemaphore.acquire();
        mBuffer->pop();
        mBufferSemaphore.release();
    }

    auto peek() -> T
    {
        assert(mBuffer != nullptr);
        
        mBufferSemaphore.acquire();
        auto output = mBuffer->peek();
        mBufferSemaphore.release();

        return output;
    }

private:
    std::unique_ptr<RingBuffer<T>> mBuffer;
    std::binary_semaphore mBufferSemaphore{0};
};

}

#endif /* SYNCRINGBUFFER_HPP */