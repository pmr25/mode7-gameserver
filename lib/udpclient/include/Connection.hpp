#ifndef CONNECTION_HPP
#define CONNECTION_HPP

namespace udpclient
{

class Connection
{
public:
    Connection() = default;
    ~Connection() = default;
};

}

#endif /* CONNECTION_HPP */
