cmake_minimum_required(VERSION 3.20)

project(udpclient CXX)

set(CMAKE_CXX_STANDARD 20)

set(
    udpclient_sources
    src/Client.cpp
    src/Connection.cpp
    src/LinuxSocket.cpp
    src/Socket.cpp
    src/SocketThread.cpp
    src/RingBuffer.cpp
    src/SyncRingBuffer.cpp
)

add_library(${PROJECT_NAME} SHARED ${udpclient_sources})
target_include_directories(${PROJECT_NAME} PRIVATE include)
target_link_libraries(${PROJECT_NAME} PRIVATE protocol)

add_executable(udpclient_integration_test src/integration_test.cpp)
target_include_directories(udpclient_integration_test PRIVATE include)
target_link_libraries(udpclient_integration_test PRIVATE udpclient protocol)

add_dependencies(${PROJECT_NAME} protocol)
add_dependencies(${PROJECT_NAME}_integration_test protocol ${PROJECT_NAME})