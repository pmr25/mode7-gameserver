#include <algorithm>
#include <iostream>
#include <memory>

#include "LinuxSocket.hpp"

auto main(int argc, char** argv) -> int
{
    std::vector<uint8_t> buffer(1024, 0);

    std::unique_ptr<udpclient::Socket> sock = std::make_unique<udpclient::LinuxSocket>("0.0.0.0", 10262);
    if (!sock->Ok())
    {
        std::cout << sock->Ok() << ":" << sock->GetLatestError() << std::endl;
    }

    const std::string msg = "hello";

    while (true)
    {
        buffer.clear();
        buffer.insert(buffer.end(), msg.begin(), msg.end());
        sock->Write(buffer);

        buffer.clear();
        sock->Read(buffer, 1024);
        std::string serverMsg = "";
        serverMsg.insert(serverMsg.end(), buffer.begin(), buffer.end());
        std::cout << serverMsg << std::endl;
    }

    return 0;
}
