#include "LinuxSocket.hpp"

#ifdef __linux__

#include <cassert>
#include <strings.h>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

namespace udpclient
{

void LinuxSocket::Open(const std::string& serverAddr, uint16_t serverPort)
{
    mSockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (mSockfd < 0)
    {
        mErrors.push_back("could not open socket");
        return;
    }

    mServer = gethostbyname(serverAddr.c_str());
    if (mServer == nullptr)
    {
        mErrors.push_back("could not create server");
        return;
    }

    mSockLen = sizeof(mServAddr);
    bzero((char*)&mServAddr, sizeof(mServAddr));
    mServAddr.sin_family = AF_INET;
    bcopy((char*)mServer->h_addr, (char*)&mServAddr.sin_addr.s_addr, mServer->h_length);
    mServAddr.sin_port = htons(serverPort);
}

void LinuxSocket::Close()
{
    close(mSockfd);
}

void LinuxSocket::Read(std::vector<uint8_t>& buffer, uint32_t amount)
{
    uint8_t tmpBuffer[mPacketSize];
    const uint32_t numRead = recvfrom(mSockfd, (void*)tmpBuffer, amount, 0, (struct sockaddr*)&mServAddr, &mSockLen);
    buffer.insert(buffer.end(), tmpBuffer, tmpBuffer + numRead);
}

void LinuxSocket::Write(const std::vector<uint8_t>& buffer)
{
    const uint32_t numRead = sendto(mSockfd, (const void*)&buffer[0], buffer.size(), 0, (struct sockaddr*)&mServAddr, mSockLen);
}

bool LinuxSocket::Ok()
{
    return mSockfd >= 0 && mServer != nullptr;
}

#endif /* __linux__ */

}
