#ifndef CRYPTO_HPP
#define CRYPTO_HPP

#include <openssl/evp.h>

class Crypto
{
public:
    unsigned char* ecdh(EVP_PKEY* clientPubKey, EVP_PKEY* serverPubKey);
};

#endif /* CRYPTO_HPP */
