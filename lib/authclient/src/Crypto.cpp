#include "Crypto.hpp"
#include <openssl/ec.h>

unsigned char* Crypto::ecdh(EVP_PKEY* clientPubKey, EVP_PKEY* serverPubKey)
{
    EVP_PKEY_CTX* clientPubKeyCtx;
    EVP_PKEY_CTX* clientSecretCtx;
    EVP_PKEY_CTX* serverPubKeyCtx;
    EVP_PKEY_CTX* sharedSecretCtx;
    EVP_PKEY_CTX* parameterGenCtx;
    EVP_PKEY* params = nullptr;
    size_t* secret_len;

    /********** Generate Parameters **********/

    // create context
    parameterGenCtx = EVP_PKEY_CTX_new_id(EVP_PKEY_EC, nullptr);
    if (parameterGenCtx == nullptr)
    {
        return nullptr;
    }
    // init parameter generation
    if (!EVP_PKEY_paramgen_init(parameterGenCtx))
    {
        return nullptr;
    }
    // set parameters
    if (EVP_PKEY_CTX_set_ec_paramgen_curve_nid(parameterGenCtx, NID_X9_62_prime256v1))
    {
        return nullptr;
    }
    // generate parameters
    if (!EVP_PKEY_paramgen(parameterGenCtx, &params))
    {
        return nullptr;
    }

    /*****************************************/

    /********** Create Client Key **********/
    
    // create shared key derivation context
    clientSecretCtx = EVP_PKEY_CTX_new(params, nullptr);
    if (clientSecretCtx == nullptr)
    {
        return nullptr;
    } 
    // init shared context
    if (EVP_PKEY_keygen_init(clientSecretCtx) != 1)
    {
        return nullptr;
    }
    if (EVP_PKEY_keygen(clientSecretCtx, &clientPubKey) != 1)
    {
        return nullptr;
    }
    
    /***************************************/

    // TODO swap keys

    /********** Shared secret **********/

    // create shared context
    sharedSecretCtx = EVP_PKEY_CTX_new(clientPubKey, nullptr);
    if (sharedSecretCtx == nullptr)
    {
        return nullptr;
    }
    // init shared context
    if (EVP_PKEY_derive_init(sharedSecretCtx) != 1)
    {
        return nullptr;
    }
    // provide peer public key
    if (EVP_PKEY_derive_set_peer(sharedSecretCtx, serverPubKey))
    {
        return nullptr;
    }
    // derive secret length
    if (EVP_PKEY_derive(sharedSecretCtx, nullptr, secret_len) != 1)
    {
        return nullptr;
    }
    unsigned char* secret;
    secret = (unsigned char*)OPENSSL_malloc(*secret_len);
    if (secret == nullptr)
    {
        return nullptr;
    }
    // derive secret
    if (EVP_PKEY_derive(sharedSecretCtx, secret, secret_len) != 1)
    {
        return nullptr;
    }

    return secret;
}
