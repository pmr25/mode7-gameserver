#include "FIFO.hpp"

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <iostream>

auto FIFO::openPipe(const std::string& rootPath, bool make) -> void
{
    std::string inPath = rootPath + "_input";
    std::string outPath = rootPath + "_output";

    if (make)
    {
        mkfifo(inPath.c_str(), 0640);
        mkfifo(outPath.c_str(), 0640);
    }

    mBridgeInputFD = open(inPath.c_str(), O_RDWR);
    mBridgeOutputFD = open(outPath.c_str(), O_RDWR);
}

auto FIFO::closePipe() -> void
{
    if (mBridgeInputFD != 0)
    {
        close(mBridgeInputFD);
        mBridgeInputFD = 0;
    }
    if (mBridgeOutputFD != 0)
    {
        close(mBridgeOutputFD);
        mBridgeOutputFD = 0;
    }
}

auto FIFO::isReady() const -> bool
{
    return mBridgeInputFD != 0 && mBridgeOutputFD != 0;
}

FIFO::operator bool() const
{
    return isReady();
}

auto FIFO::writePipe(const std::string& data) -> int
{
    return write(mBridgeInputFD, (void*)data.c_str(), data.size());
}

auto FIFO::readPipe() -> std::string
{
    std::string message = "";
    char buffer[1024];
    ssize_t numRead;
    char lastChar = 0;

    do
    {
        numRead = read(mBridgeOutputFD, buffer, 1024);
        if (numRead == -1)
        {
            std::cout << "error: " << strerror(errno) << std::endl;
            return "";
        }
        lastChar = buffer[numRead-1];
        message += std::string(buffer, numRead);
    }
    while (numRead > 0 && lastChar != '\n');

    return message;
}
