#ifndef FIFO_HPP
#define FIFO_HPP

#include <string>

class FIFO
{
public:
    FIFO(const std::string& rootPath, bool make = false)
    {
        openPipe(rootPath, make);
    }

    ~FIFO()
    {
        closePipe();
    }

    auto openPipe(const std::string& rootPath, bool make) -> void;
    auto closePipe() -> void;
    auto isReady() const -> bool;
    operator bool() const;

    auto writePipe(const std::string& data) -> int;
    auto readPipe() -> std::string;

private:
    int mBridgeInputFD{0};
    int mBridgeOutputFD{0};
};

#endif /* FIFO_HPP */
