#include "FIFO.hpp"

#include <iostream>

auto main() -> int
{
    auto pipe = FIFO("/tmp/authbridgep");
    if (!pipe)
    {
        std::cout << "error" << std::endl;
        return 1;
    }

    for (;;)
    {
        pipe.writePipe("test\n");
        std::cout << pipe.readPipe() << std::endl;
    }

    return 0;
}
