//go:build bridge

package main

import (
	"flag"
	"fmt"

	"gitlab.com/pmr25/mode7-gameserver/internal/handler"

	"gitlab.com/pmr25/mode7-gameserver/internal/app"
	"gitlab.com/pmr25/mode7-gameserver/internal/service"
	"go.uber.org/fx"
	"go.uber.org/zap"
)

var appFlag = flag.String("app", "", "the app to run")

func main() {
	flag.Parse()

	deps := fx.Provide(
		zap.NewExample,

		handler.NewBridgeAuthHandler,
		// handler.NewBridgeGameHandler,

		service.NewBridgeAuthService,
		service.NewBridgeGameService,
		fx.Annotate(service.NewCryptoService, fx.As(new(service.CryptoInterface))),

		app.NewAuthBridge,
	)

	var apps fx.Option = nil

	if *appFlag == "auth" {
		apps = fx.Invoke(app.NewAuthBridge)
	} else if *appFlag == "game" {
		apps = fx.Invoke(
			app.NewGameBridge,
		)
	} else {
		fmt.Println("unknown app")
	}

	if apps == nil {
		return
	}

	fx.New(
		deps,
		apps,
	).Run()
}
